# Java_Mail_App

Java application for Gmail

This Maven project is a Java based client for reading, sending & managing email using a Gmail account. 
The application supports the English and French languages, and it is opened in the appropriate language depending on what it is set to in your computer's settings.
!!If you wish to change the language, you will have to exit the app, change your PC's display language settings, logout and login your Windows account and open the app again.
The first time you open the app, you will be prompted to enter your email address and password.
Before registering the email address, the basic configurations in the section below must be set. If you wish to keep the default configuration, simply click on the 'Save the configuration' button
After checking for validity of the account, you have entered the main component of the application.

To create a new email: 2 ways
1- In the menu bar, click on 'File' --> 'New email'
2- In the toolbar, click on the icon with the mail enveloppe and the pencil
It will open a form and an HTML editor in the bottom right section of the application.

To send an email: 2 ways
1- Given that the email editor is opened in the app, in the menu bar, click on 'Email' --> 'Send'
!!If the editor is not opened this option will be disabled
2- In the toolbar above the form, click on the paper plane icon

To add regular/embedded attachments: 2 ways
1- Given that the email editor is opened in the app, in the menu bar, click on 'Email' --> 'New Attachment' --> Regular/Embedded attachment
!!If the editor is not opened this option will be disabled
2- In the toolbar above the form, click on the paper clip icon with a '+' --> Regular/Embedded attachment

In both cases the file explorer will be opened, which will allow you to chose an image file.

To remove attachments: 2 ways (Option is only enabled if an attachment has been added to the email)
1- Given that the email editor is opened in the app, in the menu bar, click on 'Email' --> Remove an attachment --> Click on the attachment you want to remove
!!If the editor is not opened this option will be disabled
2- In the toolbar above the form, click on the paper clip icon with a '-' --> Click on the attachment you want to remove
!!In both cases, in the list of attachments, individual attachments will have icons next to the file name to represent if it is an embedded attachment or a regular one.
!!!For embedded attachments, you will have to erase it from the HTML editor as well after removing it using this function.

To create a new folder: 2 ways
1- In the menu bar, click on File --> New Folder --> Enter the name of the folder you wish to create and click on the 'Create folder' button
2- In the top toolbar, click on the icon with the folder and a '+' and enter the same information as described in way #1.
!!The application will automatically check for folder validity and will present you with the appropriate error message if there is an issue.

To refresh emails: 2 ways
1- In the menu bar, click on Email --> Refresh emails
!!When refreshing is finished, it will display a message saying how many emails were retrieved or if mailbox is already up to date.
2- In the top toolbar, click on the icon with the 2 rotating arrows.

To open a folder:
Simply click on one of the folder items in the left list to open a list of its contained emails in the table in the top right section of the app.

To delete a folder:
In the left list of folders, right click any folder other than 'sent' and 'received' to open a Context menu --> Click on 'Delete'
You will be prompted with a confirmation message to confirm that you want to delete this folder.

To rename a folder:
In the left list of folders, right click any folder other than 'sent' and 'received' to open a Context menu --> Click on 'Rename'
You will be prompted to enter the new name of the folder, and when this is set, click on 'Rename'
!!The application will automatically check for folder validity and will present you with the appropriate error message if there is an issue.

To open an email:
Given that a folder which contains emails is opened, navigate to the top right section of the application and click on any row in the table to open an email in the section below.

To open an attachment (regular):
Given that an email has been opened in the bottom right section of the application and that this email contains an attachment, it will be accessible in the bottom next to the 'Open attachment' label
If the opened email has no attachment, the combo box will be greyed out (disabled) with label 'No attachment'
If the opened email has one or more attachments, click on the Combo box --> Click on the file you want to open

To save an attachment (regular):
When a file is selected, it is opened in a new window which shows the attachment, the file name and an option to save the attachment to your computer. To do so, click on the button with the down arrow and select a folder on your PC to save it.

To search for an email by subject:
Click on the text box above the table of emails and start typing the name of the subject you are looking for. As you are typing, results relevant to your search criteria will show up.
!!When typing and erasing everything afterwards, you will be able to filter for emails with empty subjects.
!!!To reset search results, switch to another folder and come back to the folder where you were and all emails will be there.

To sort for specific fields:
Click on the table headings to sort by those fields.
Up arrow => ascending
Down arrow => descending
No arrow => default sort

To delete an email:
Right click the email opened in the table of emails that you wish to delete to show a Context menu --> Delete email
You will be prompted to confirm that you really want to delete this email.

To move an email to another folder:
Right click the email opened in the table of emails that you wish to move to show a Context menu --> Move email to... --> Click on the folder where you want to move the email to
You will be prompted to confirm that you really want to move this email.

To change email accounts/change configuration:
In the menu bar, click Edit --> Application configuration
When registering a new account, the application will use the new credentials from now on to send/receive emails

To access information about the application (About):
In the menu bar, click Help --> About
A message will popup with some information concerning the app and its conception.