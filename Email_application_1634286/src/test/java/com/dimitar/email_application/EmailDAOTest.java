package com.dimitar.email_application;

import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import com.dimitar.persistence.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.DriverManager;
import java.util.Scanner;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import jodd.mail.EmailAddress;
import static org.junit.Assert.assertEquals;

/**
 * The class for testing all EmailDAO and related functionality
 * 
 * @author Dimitar
 */
public class EmailDAOTest {
    private final static String URL = "jdbc:mysql://localhost:3306/EMAIL?zeroDateTimeBehavior=CONVERT_TO_NULL";
    private final static String USER = "dimitar";
    private final static String PASSWORD = "dimitar";
    
    private final EmailDAO emailDAO = new EmailDAOImpl();
    private final AddressDAO addressDAO = new AddressDAOImpl();
    private final FolderDAO folderDAO = new FolderDAOImpl();
    private final EmailAddressDAO emailAddressDAO = new EmailAddressDAOImpl();
    private final AttachmentDAO attachmentDAO = new AttachmentDAOImpl();
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOTest.class);
    
    private final DatabaseSeeder seeder = new DatabaseSeeder();
    
    @Before
    public void seedDatabase() throws SQLException, IOException{
        LOG.info("@Before seeding");
        if(seeder == null){
            LOG.info("NULL SEEDER");
        }
        seeder.seed();
    }
    
    @Test
    public void findIDOfAddressTest() throws SQLException{
        int foundID;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            foundID = addressDAO.findIDOfAddress("send.1634286@gmail.com", connection);
        }
        assertEquals(1, foundID);
    }
    
    @Test
    public void findCountOfAddressTest() throws SQLException{
        int count;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            count = addressDAO.findCountOfAddress("send.1634286@gmail.com", connection);
        }
        assertEquals(1, count);
    }
    
    @Test
    public void createAddressTest() throws SQLException{
        int createdCount;
        int addressCreatedID;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            createdCount = addressDAO.createAddress("misc@gmail.com", "Misc", connection);
            addressCreatedID = addressDAO.findIDOfAddress("misc@gmail.com", connection);
        }
        if(createdCount != 1){
            Assert.fail();
        }
        else{
            assertEquals(5, addressCreatedID);
        }
    }
    
    @Test
    public void createAddressAlreadyExistingTest() throws SQLException{
        int createdCount;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            createdCount = addressDAO.createAddress("send.1634286@gmail.com", "Dimitar", connection);
        }
        assertEquals(0, createdCount);
    }
    
    @Test
    public void findAllFoldersTest() throws SQLException{
        List<String> foldersFound;
        int countFolders;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            foldersFound = folderDAO.findAllFolders(connection);
            countFolders = foldersFound.size();
        }
        if(countFolders != 3){
            Assert.fail();
        }
        else{
            assertEquals("other", foldersFound.get(2));
        }
    }
    @Test
    public void renameFolderTest() throws SQLException{
        int renamedCount;
        String newName;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            renamedCount = folderDAO.renameFolder("other", "misc", connection);
            newName = folderDAO.findAllFolders(connection).get(2);
        }
        if(renamedCount != 1){
            Assert.fail();
        }
        else{
            assertEquals("misc", newName);
        }
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void renameFolderSentTest() throws SQLException{
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            folderDAO.renameFolder("sent", "sent", connection);
        }
        Assert.fail();
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void renameFolderSameNameTest() throws SQLException{
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            folderDAO.renameFolder("other", "other", connection);
        }
        Assert.fail();
    }
    
    @Test
    public void renameUnexistingFolderTest() throws SQLException{
        int countRenamed;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            countRenamed = folderDAO.renameFolder("misc", "other", connection);
        }
        assertEquals(0, countRenamed);
    }
    
    @Test
    public void createFolderTest() throws SQLException{
        int createdCount;
        String name;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            createdCount = folderDAO.createFolder("misc", connection);
            name = folderDAO.findAllFolders(connection).get(3);
        }
        if(createdCount != 1){
            Assert.fail();
        }
        else{
            assertEquals("misc", name);
        }
    }
    
    @Test (expected = SQLException.class)
    public void createExistingFolerTest() throws SQLException{
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            folderDAO.createFolder("sent", connection);
        }
        Assert.fail();
    }
    
    @Test
    public void deleteFolderTest() throws SQLException, IOException{
        int deletedCount;
        int folderCount;
        List<EmailData> emails;
        int emailCount;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            deletedCount = folderDAO.deleteFolder("other", connection);
            folderCount = folderDAO.findAllFolders(connection).size();
            emails = emailDAO.findAll();
            emailCount = emails.size();
        }
        if(deletedCount != 1){
            Assert.fail();
        }
        else if(folderCount != 2){
            Assert.fail();
        }
        else{
            assertEquals(22, emailCount);
        }
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void deleteSentFolderTest() throws SQLException{
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            folderDAO.deleteFolder("sent", connection);
        }
        Assert.fail();
    }
    
    @Test
    public void createEmailAddressEntryTest() throws SQLException{
        int emailAddressEntriesCount;
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            emailAddressEntriesCount = emailAddressDAO.createEmailAddressEntry(17, "other.1634286@gmail.com", "CC", connection);
        }
        assertEquals(1, emailAddressEntriesCount);
    }
    
    @Test
    public void createAttachmentTest() throws SQLException, IOException{
        int attachmentCount;
        BufferedImage image = ImageIO.read(new File("FreeFall.jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bos);
        byte[] imgData = bos.toByteArray();
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            attachmentCount = attachmentDAO.createAttachment(17, "FreeFall.jpg", imgData, "regular", connection);
        }
        assertEquals(1, attachmentCount);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void createInvalidAttachmentTypeTest() throws SQLException, IOException{
        BufferedImage image = ImageIO.read(new File("FreeFall.jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bos);
        byte[] imgData = bos.toByteArray();
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            attachmentDAO.createAttachment(17, "FreeFall.jpg", imgData, "err", connection);
        }
        Assert.fail();
    }
    
    @Test
    public void findIDTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("Daltfresh");
        emailBeanExpected.setID(1);
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setHtmlMessage("");
        emailBeanExpected.setTextMessage("");
        emailBeanExpected.setFolderName("sent");
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        EmailAddress [] cc = new EmailAddress[1];
        EmailAddress other = new EmailAddress("Dimitrios", "other.1634286@gmail.com");
        cc[0] = other;
        emailBeanExpected.setCC(cc);
        EmailAddress [] bcc = new EmailAddress[1];
        EmailAddress otherBCC = new EmailAddress("ninja", "qwertylemen@gmail.com");
        bcc[0] = otherBCC;
        emailBeanExpected.setBCC(bcc);
        AttachmentData attach = new AttachmentData("WindsorKen180.jpg", null);
        AttachmentData[] attachArr = {attach};
        emailBeanExpected.setAttachments(attachArr);
        EmailData emailBeanReceived = emailDAO.findID(1);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void findFolderTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("Sonsing");
        emailBeanExpected.setID(15);
        emailBeanExpected.setFrom("bcopemane@delicious.com");
        emailBeanExpected.setHtmlMessage("Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.");
        emailBeanExpected.setTextMessage("Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.");
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        EmailAddress [] cc = new EmailAddress[1];
        EmailAddress other = new EmailAddress("Dimitar", "send.1634286@gmail.com");
        cc[0] = other;
        emailBeanExpected.setCC(cc);
        List<EmailData> emails = emailDAO.findFolder("other");
        if(emails.size() != 3){
            Assert.fail();
        }
        EmailData emailBeanReceived = emails.get(0);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void findSubjectTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("");
        emailBeanExpected.setID(10);
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setHtmlMessage("<html><META http-equiv=Content-Typecontent='text/html; charset=utf-8'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>");
        emailBeanExpected.setTextMessage("Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et spendisse potenti.");
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        List<EmailData> emails = emailDAO.findSubject("");
        if(emails.size() != 3){
            Assert.fail();
        }
        EmailData emailBeanReceived = emails.get(0);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void findAllTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("Daltfresh");
        emailBeanExpected.setID(1);
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setHtmlMessage("");
        emailBeanExpected.setTextMessage("");
        emailBeanExpected.setFolderName("sent");
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        EmailAddress [] cc = new EmailAddress[1];
        EmailAddress other = new EmailAddress("Dimitrios", "other.1634286@gmail.com");
        cc[0] = other;
        emailBeanExpected.setCC(cc);
        EmailAddress [] bcc = new EmailAddress[1];
        EmailAddress otherBCC = new EmailAddress("ninja", "qwertylemen@gmail.com");
        bcc[0] = otherBCC;
        emailBeanExpected.setBCC(bcc);
        AttachmentData attach = new AttachmentData("WindsorKen180.jpg", null);
        AttachmentData[] attachArr = {attach};
        emailBeanExpected.setAttachments(attachArr);
        List<EmailData> emails = emailDAO.findAll();
        if(emails.size() != 25){
            Assert.fail();
        }
        EmailData emailBeanReceived = emails.get(0);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void createBasicEmailTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("");
        emailBeanExpected.setHtmlMessage("");
        emailBeanExpected.setTextMessage("");
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setFolderName("sent");
        emailBeanExpected.setID(26);
        emailBeanExpected.setMessagePriority(1);
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        emailBeanExpected.setSendTime(LocalDateTime.now());
        emailDAO.create(emailBeanExpected);
        EmailData emailBeanReceived = emailDAO.findID(26);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void createEmailCCBCCTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("");
        emailBeanExpected.setHtmlMessage("");
        emailBeanExpected.setTextMessage("");
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setFolderName("sent");
        emailBeanExpected.setID(26);
        emailBeanExpected.setMessagePriority(1);
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        EmailAddress [] cc = new EmailAddress[1];
        EmailAddress other = new EmailAddress("Dimitrios", "other.1634286@gmail.com");
        cc[0] = other;
        emailBeanExpected.setCC(cc);
        EmailAddress [] bcc = new EmailAddress[1];
        EmailAddress otherBCC = new EmailAddress("new", "new@mail.com");
        bcc[0] = otherBCC;
        emailBeanExpected.setBCC(bcc);
        emailBeanExpected.setSendTime(LocalDateTime.now());
        emailDAO.create(emailBeanExpected);
        EmailData emailBeanReceived = emailDAO.findID(26);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void createEmailWAttachmentsTest() throws SQLException, IOException{
        EmailData emailBeanExpected = new EmailData();
        emailBeanExpected.setSubject("dasfasdas");
        emailBeanExpected.setHtmlMessage("dsa");
        emailBeanExpected.setTextMessage("das");
        emailBeanExpected.setFrom("send.1634286@gmail.com");
        emailBeanExpected.setFolderName("sent");
        emailBeanExpected.setID(26);
        emailBeanExpected.setMessagePriority(1);
        EmailAddress[] to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dmitry", "receive.1634286@gmail.com");
        to[0] = recip;
        emailBeanExpected.setTo(to);
        EmailAddress [] cc = new EmailAddress[1];
        EmailAddress other = new EmailAddress("Dimitrios", "other.1634286@gmail.com");
        cc[0] = other;
        emailBeanExpected.setCC(cc);
        EmailAddress [] bcc = new EmailAddress[2];
        EmailAddress otherBCC = new EmailAddress("new", "new@mail.com");
        EmailAddress otherBCC2 = new EmailAddress("ninja", "qwertylemen@gmail.com");
        bcc[0] = otherBCC;
        bcc[1] = otherBCC2;
        emailBeanExpected.setBCC(bcc);
        BufferedImage image = ImageIO.read(new File("FreeFall.jpg"));
        BufferedImage image2 = ImageIO.read(new File("WindsorKen180.jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bos);
        ImageIO.write(image2, "jpg", bos2);
        byte[] imgData = bos.toByteArray();
        byte[] imgData2 = bos2.toByteArray();
        AttachmentData attach1 = new AttachmentData("WindsorKen180.jpg", imgData2);
        AttachmentData attach2 = new AttachmentData("FreeFall.jpg", imgData);
        AttachmentData[] regAttach = {attach1, attach2};
        AttachmentData[] embeddAttach = {attach2};
        emailBeanExpected.setAttachments(regAttach);
        emailBeanExpected.setEmbeddedAttachments(embeddAttach);
        emailBeanExpected.setSendTime(LocalDateTime.now());
        emailDAO.create(emailBeanExpected);
        EmailData emailBeanReceived = emailDAO.findID(26);
        assertEquals(0, emailBeanExpected.compareTo(emailBeanReceived));
    }
    
    @Test
    public void deleteEmailTest() throws SQLException, IOException{
        int deletedCount = emailDAO.delete(4);
        if(deletedCount != 1){
            Assert.fail();
        }
        else{
            EmailData foundEmail = emailDAO.findID(4);
            assertEquals(null, foundEmail);
        }
    }
    
    @AfterClass
    public static void seedDBAfterTestCompletion() throws SQLException, IOException{
        LOG.info("@AfterClass seeding");
        new EmailDAOTest().seedDatabase();
    }
    
}
