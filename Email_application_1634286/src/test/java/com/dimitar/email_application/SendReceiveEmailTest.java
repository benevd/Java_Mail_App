/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dimitar.email_application;

import com.dimitar.business.SendEmail;
import com.dimitar.business.ReceiveEmail;
import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import jodd.mail.EmailAddress;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class that runs test cases for sending emails and receiving them
 * @author Dimitar
 */
public class SendReceiveEmailTest {
    //Objects that will be used in tests
    private EmailData emailBean;
    private AttachmentData embeddedAttachment;
    private AttachmentData attachment;
    private SendEmail send;
    private ReceiveEmail receive;
    private String from;
    private EmailAddress[] to;
    private String subject;
    private String textMessage;
    private String htmlMessage;
    private AttachmentData[] embeddedAttachments;
    private AttachmentData[] attachments;
    
    //Before every test
    @Before
    public void init(){
        from = "send.1634286@gmail.com";
        to = new EmailAddress[1];
        EmailAddress recip = new EmailAddress("Dimitar", "receive.1634286@gmail.com");
        to[0] = recip;
        subject = "Test";
        textMessage = "This is plain text message";
        htmlMessage = "<html><META http-equiv=Content-Type"+
                "content=\"text/html; charset=utf-8\">"+
                "<body><h1>HTML Message</h1>"+
                "<h2>Here is some text in the HTML message</h2>"+
                "<h1>Here is my photograph embedded in " +
                "this email.</h1><img src='cid:FreeFall.jpg'>" +
                "<h2>I'm flying!</h2></body></html>";
        send = new SendEmail();
        send.setPassword("joddmailproject1634286");
        send.setSmtpServer("smtp.gmail.com");
        receive = new ReceiveEmail();
        receive.setCredentials("receive.1634286@gmail.com", "joddmailproject1634286");
        receive.setImapServer("imap.gmail.com");
        emailBean = new EmailData(from, to, null, subject, textMessage, htmlMessage, null, null, null, null, 0);
    }
    //******TESTS THAT ARE NOT SUPPOSED TO THROW EXCEPTIONS******
    //Sending only a plain text message
    @Test
    public void emailTextMessageOnlyTest() throws InterruptedException{
        emailBean.setHtmlMessage("");
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
         Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending only with CC
    @Test
    public void emailCCTest() throws InterruptedException{
        EmailAddress [] CC = new EmailAddress[1];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        CC[0] = other;
        emailBean.setCC(CC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with CC and BCC
    @Test
    public void emailCCBCCTest() throws InterruptedException{
        EmailAddress [] CC = new EmailAddress[1];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        CC[0] = other;
        EmailAddress [] BCC = new EmailAddress[1];
        EmailAddress otherBCC = new EmailAddress("other", "send.1634286@gmail.com");
        BCC[0] = otherBCC;
        emailBean.setCC(CC);
        emailBean.setBCC(BCC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending only with BCC
    @Test
    public void emailBCCTest() throws InterruptedException{
        EmailAddress [] BCC = new EmailAddress[1];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        BCC[0] = other;
        emailBean.setBCC(BCC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending only with multiple CC
    @Test
    public void emailMultipleCCTest() throws InterruptedException{
        EmailAddress [] CC = new EmailAddress[2];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress otherCC = new EmailAddress("send", "send.1634286@gmail.com");
        CC[0] = other;
        CC[1] = otherCC;
        emailBean.setCC(CC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    
    //Sending only with multiple BCC
    @Test
    public void emailMultipleBCCTest() throws InterruptedException{
        EmailAddress [] BCC = new EmailAddress[2];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress otherBCC = new EmailAddress("send", "send.1634286@gmail.com");
        BCC[0] = other;
        BCC[1] = otherBCC;
        emailBean.setBCC(BCC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with multiple CC and multiple BCC
    @Test
    public void emailMultipleCCBCCTest() throws InterruptedException{
        EmailAddress [] CC = new EmailAddress[2];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress otherCC = new EmailAddress("send", "send.1634286@gmail.com");
        CC[0] = other;
        CC[1] = otherCC;
        emailBean.setCC(CC);
        EmailAddress [] BCC = new EmailAddress[2];
        BCC[0] = other;
        BCC[1] = otherCC;
        emailBean.setBCC(BCC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    
    //Sending only with embedded attachment
    @Test
    public void emailEmbeddedAttachmentTest() throws InterruptedException{
        embeddedAttachments = new AttachmentData[1];
        embeddedAttachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        embeddedAttachments[0] = embeddedAttachment;
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending only with regular attachment
    @Test
    public void emailRegularAttachmentTest() throws InterruptedException{
        attachments = new AttachmentData[1];
        attachment = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        attachments[0] = attachment;
        emailBean.setAttachments(attachments);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with embedded and regular attachments
    @Test
    public void emailRegularEmbeddedAttachmentTest() throws InterruptedException{
        embeddedAttachments = new AttachmentData[1];
        embeddedAttachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        embeddedAttachments[0] = embeddedAttachment;
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        attachments = new AttachmentData[1];
        attachment = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        attachments[0] = attachment;
        emailBean.setAttachments(attachments);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with CC, BCC, attachment and embedded attachment
    @Test
    public void emailRegularEmbeddedAttachmentCCBCCTest() throws InterruptedException{
        embeddedAttachments = new AttachmentData[1];
        embeddedAttachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        embeddedAttachments[0] = embeddedAttachment;
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        attachments = new AttachmentData[1];
        attachment = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        attachments[0] = attachment;
        emailBean.setAttachments(attachments);
        EmailAddress [] CC = new EmailAddress[1];
        EmailAddress other = new EmailAddress("other", "other.1634286@gmail.com");
        CC[0] = other;
        EmailAddress [] BCC = new EmailAddress[1];
        EmailAddress sender = new EmailAddress("send", "send.1634286@gmail.com");
        BCC[0] = sender;
        emailBean.setCC(CC);
        emailBean.setBCC(BCC);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending  with multiple embedded attachments
    @Test
    public void emailMultipleEmbeddedAttachmentTest() throws InterruptedException{
        embeddedAttachments = new AttachmentData[2];
        embeddedAttachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        AttachmentData embeddedAttachment2 = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        embeddedAttachments[0] = embeddedAttachment;
        embeddedAttachments[1] = embeddedAttachment2;
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        emailBean.setHtmlMessage("<html><META http-equiv=Content-Type"+
                "content=\"text/html; charset=utf-8\">"+
                "<body><h1>HTML Message</h1>"+
                "<h2>Here is some text in the HTML message</h2>"+
                "<h1>Here is my photograph embedded in " +
                "this email.</h1><img src='cid:FreeFall.jpg'>" +
                "<img src='cid:WindsorKen180.jpg'>"+
                "<h2>I'm flying!</h2></body></html>");
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with multiple regular attachments
    @Test
    public void emailMultipleRegularAttachmentTest() throws InterruptedException{
        attachments = new AttachmentData[2];
        attachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        AttachmentData attachment2 = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        attachments[0] = attachment;
        attachments[1] = attachment2;
        emailBean.setAttachments(attachments);
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending with multiple embedded and regular attachments
    @Test
    public void emailMultipleEmbeddedRegularAttachmentTest() throws InterruptedException{
        embeddedAttachments = new AttachmentData[2];
        embeddedAttachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        AttachmentData embeddedAttachment2 = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        embeddedAttachments[0] = embeddedAttachment;
        embeddedAttachments[1] = embeddedAttachment2;
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        attachments = new AttachmentData[2];
        attachment = new AttachmentData("FreeFall.jpg", "FreeFall.jpg", null);
        AttachmentData attachment2 = new AttachmentData("WindsorKen180.jpg", "WindsorKen180.jpg", null);
        attachments[0] = attachment;
        attachments[1] = attachment2;
        emailBean.setAttachments(attachments);
        emailBean.setHtmlMessage("<html><META http-equiv=Content-Type"+
                "content=\"text/html; charset=utf-8\">"+
                "<body><h1>HTML Message</h1>"+
                "<h2>Here is some text in the HTML message</h2>"+
                "<h1>Here is my photograph embedded in " +
                "this email.</h1><img src='cid:FreeFall.jpg'>" +
                "<img src='cid:WindsorKen180.jpg'>"+
                "<h2>I'm flying!</h2></body></html>");
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(0, emailBean.compareTo(emailsReceived[0]));
    }
    //Sending multiple emails
    @Test
    public void multipleEmailSendTest() throws InterruptedException{
        emailBean.setHtmlMessage("");
        send.sendEmail(emailBean);
        emailBean.setHtmlMessage("html test 2");
        send.sendEmail(emailBean);
        // Add a five second pause to allow the Gmail server to receive what has
        // been sent
        Thread.sleep(5000);
        EmailData[] emailsReceived = receive.receiveEmail();
        assertEquals(2, emailsReceived.length);
    }
    
    //******TESTS THAT ARE SUPPOSED TO THROW EXCEPIONS******
    //Sending with no sender
    @Test(expected = IllegalArgumentException.class)
    public void emailNoSender(){
        emailBean.setFrom(null);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    //Sending with a bad sender
    @Test(expected = IllegalArgumentException.class)
    public void emailBadSender(){
        emailBean.setFrom("bad@");
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }

    //Sending with no recipients
    @Test(expected = IllegalArgumentException.class)
    public void emailNoRecipient(){
        emailBean.setTo(null);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    //Sending with a bad recipient
    @Test(expected = IllegalArgumentException.class)
    public void emailBadRecipient(){
        EmailAddress [] recipients = new EmailAddress[2];
        EmailAddress recip1 = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress recip2 = new EmailAddress("bad", "bad@");
        recipients[0] = recip1;
        recipients[1] = recip2;
        emailBean.setTo(recipients);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    //Sending with a bad CC
    @Test(expected = IllegalArgumentException.class)
    public void emailBadCC(){
        EmailAddress [] CCS = new EmailAddress[2];
        EmailAddress cc1 = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress cc2 = new EmailAddress("bad", "bad@");
        CCS[0] = cc1;
        CCS[1] = cc2;
        emailBean.setCC(CCS);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    //Sending with a bad BCC
    @Test(expected = IllegalArgumentException.class)
    public void emailBadBCC(){
        EmailAddress [] BCCS = new EmailAddress[2];
        EmailAddress bcc1 = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress bcc2 = new EmailAddress("bad", "bad@");
        BCCS[0] = bcc1;
        BCCS[1] = bcc2;
        emailBean.setBCC(BCCS);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    
    //Sending with both bad CC and BCC
    @Test(expected = IllegalArgumentException.class)
    public void emailBadCCBCC(){
        EmailAddress [] CCS = new EmailAddress[2];
        EmailAddress cc1 = new EmailAddress("other", "other.1634286@gmail.com");
        EmailAddress cc2 = new EmailAddress("bad", "bad@");
        CCS[0] = cc1;
        CCS[1] = cc2;
        emailBean.setCC(CCS);
        EmailAddress [] BCCS = new EmailAddress[2];
        BCCS[0] = cc1;
        BCCS[1] = cc2;
        emailBean.setBCC(BCCS);
        send.sendEmail(emailBean);
        Assert.fail("No exception was thrown");
    }
    
}
