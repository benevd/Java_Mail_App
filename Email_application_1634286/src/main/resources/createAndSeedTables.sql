--CREATE & SEED FOLDER
DROP TABLE IF EXISTS Folder;

CREATE TABLE Folder (
    folder_id int NOT NULL AUTO_INCREMENT,
    name varchar(30) NOT NULL UNIQUE,
    CONSTRAINT Folder_pk PRIMARY KEY (folder_id)
);

INSERT INTO Folder (name) VALUES
("sent"),
("received"),
("other");

--CREATE & SEED EMAIL
DROP TABLE IF EXISTS Email;

CREATE TABLE Email (
    email_id int NOT NULL AUTO_INCREMENT,
    subject varchar(60) NOT NULL,
    sender varchar(60) NOT NULL,
    html_msg varchar(10000) NOT NULL,
    text_msg varchar(5000) NOT NULL,
    sent_time timestamp NOT NULL,
    received_time timestamp,
    msg_priority int NOT NULL,
    folder_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (email_id)
);

-- Reference: Email_Folder (table: Email)
ALTER TABLE Email ADD CONSTRAINT Email_Folder FOREIGN KEY Email_Folder (folder_id)
    REFERENCES Folder (folder_id) ON DELETE CASCADE;

INSERT INTO Email (subject, sender, html_msg, text_msg, sent_time, received_time, msg_priority, folder_id) VALUES
("Daltfresh", "send.1634286@gmail.com", "", "", "2017-11-20 23:01:06", NULL, 1, 1),
("Bytecard", "send.1634286@gmail.com", "", "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.", "2017-09-27 03:00:36", NULL, 1, 1),
("Tin", "send.1634286@gmail.com", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "elementum.", "2017-12-22 06:22:28", NULL, 1, 1),
("Bitwolf", "dstoite3@businessweek.com", "Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.", "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.", "2018-05-25 00:17:31", "2017-11-15 00:10:41", 1, 2),
("Matsoft", "send.1634286@gmail.com", "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.", "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.", "2018-01-20 04:18:04", NULL, 1, 1),
("Job", "send.1634286@gmail.com", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.", "2017-12-26 07:01:14", NULL, 1, 1),
("Fix San", "send.1634286@gmail.com", "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.", "", "2018-01-01 12:44:30", NULL, 1, 1),
("Vagram", "send.1634286@gmail.com", "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.", "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.", "2018-07-01 15:49:43", NULL, 1, 1),
("Sonsing", "cmissington8@dedecms.com", "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet apien non mi. Integer ac neque.", "2018-06-30 15:24:51", "2018-03-05 12:58:16", 1, 2),
("", "send.1634286@gmail.com", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et spendisse potenti.", "2018-09-11 03:00:06", NULL, 1, 1),
("Kanlam", "send.1634286@gmail.com", "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", "2018-06-08 17:40:58", NULL, 1, 1),
("Quo Lux", "gconneelyb@mozilla.org", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><img src='cid:WindsorKen180.jpg'><h2>I'm flying!</h2></body></html>", "Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.", "2017-09-23 03:06:20", "2018-08-05 11:57:20", 1, 2),
("Konklab", "send.1634286@gmail.com", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.", "2018-02-20 08:17:17", "2018-01-26 00:24:37", 1, 1),
("", "send.1634286@gmail.com", "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.", "Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.", "2017-10-22 00:02:08", NULL, 1, 1),
("Sonsing", "bcopemane@delicious.com", "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.", "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.", "2018-03-16 03:41:59", "2018-08-15 10:28:40", 1, 3),
("Latlux", "send.1634286@gmail.com", "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.", "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.", "2018-06-29 05:49:57", NULL, 1, 1),
("Stringtough", "send.1634286@gmail.com", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.", "2017-09-29 23:37:31", NULL, 1, 1),
("Treeflex", "send.1634286@gmail.com", "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.", "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.", "2018-08-27 09:14:10", NULL, 1, 1),
("Pannier", "send.1634286@gmail.com", "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.", "Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.", "2018-04-22 05:45:55", NULL, 1, 1),
("", "bbroughamj@woothemes.com", "Proin inte, hendrerit at, vulputate vitae, nisl.", "Quisque rutrum, nulla. Nunc purus.", "2018-04-20 20:35:03", "2018-01-26 16:29:25", 1, 2),
("Sonsing", "ggoodlipk@psu.edu", "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.", "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.", "2017-10-27 17:11:35", "2018-04-09 06:45:46", 1, 2),
("Rank", "callwrightl@usda.gov", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>","", "2018-04-07 11:03:22", "2018-07-24 16:28:11", 1, 2),
("Zaam-Dox", "send.1634286@gmail.com", "Sed ante. Vivamus tortor. Duis mattis egestas metus.", "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.", "2018-02-14 06:07:49", NULL, 1, 1),
("Ventosanzap", "fgerramn@toplist.cz", "", "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.", "2018-01-30 10:57:12", "2018-04-05 19:35:41", 1, 3),
("Asoka", "lkopkeo@sakura.ne.jp", "<html><META http-equiv=Content-Typecontent=\'text/html; charset=utf-8\'><body><h1>HTML Message</h1><h2>Here is some text in the HTML message</h2><h1>Here is my photograph embedded in this email.</h1><img src='cid:FreeFall.jpg'><h2>I'm flying!</h2></body></html>", "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.", "2018-07-24 03:02:52", "2018-04-14 04:36:34", 1, 3);

--CREATE & SEED ADDRESS
DROP TABLE IF EXISTS Address;

CREATE TABLE Address (
    address_id int NOT NULL AUTO_INCREMENT,
    email_addr varchar(60) NOT NULL UNIQUE,
    personal_name varchar(60),
    CONSTRAINT Address_pk PRIMARY KEY (address_id)
);

INSERT INTO Address (email_addr, personal_name) VALUES 
("send.1634286@gmail.com", "Dimitar"),
("receive.1634286@gmail.com", "Dmitry"),
("other.1634286@gmail.com", "Dimitrios"),
("qwertylemen@gmail.com", "ninja");

--CREATE & SEED ATTACHMENT
DROP TABLE IF EXISTS Attachment;

CREATE TABLE Attachment (
    attach_id int NOT NULL AUTO_INCREMENT,
    file_name varchar(50) NOT NULL,
    content blob NOT NULL,
    attach_type varchar(10) NOT NULL,
    email_id int NOT NULL,
    CONSTRAINT id PRIMARY KEY (attach_id)
);

-- Reference: Attachment_Email (table: Attachment)
ALTER TABLE Attachment ADD CONSTRAINT Attachment_Email FOREIGN KEY Attachment_Email (email_id)
    REFERENCES Email (email_id) ON DELETE CASCADE;

--CREATE AND SEED EMAIL_ADDRESS
DROP TABLE IF EXISTS Email_address;

CREATE TABLE Email_address (
    email_id int NOT NULL,
    address_id int NOT NULL,
    type varchar(3) NOT NULL
);

-- Reference: Address_Email_address (table: Email_address)
ALTER TABLE Email_address ADD CONSTRAINT Address_Email_addresse FOREIGN KEY Address_Email_address (address_id)
    REFERENCES Address (address_id);

-- Reference: Email_addresse_Email (table: Email_address)
ALTER TABLE Email_address ADD CONSTRAINT Email_addresse_Email FOREIGN KEY Email_addresse_Email (email_id)
    REFERENCES Email (email_id) ON DELETE CASCADE;

INSERT INTO Email_address (email_id, address_id, type) VALUES
(1, 2, "TO"),
(1, 3, "CC"),
(1, 4, "BCC"),
(2, 2, "TO"),
(3, 2, "TO"),
(4, 1, "TO"),
(5, 2, "TO"),
(5, 3, "CC"),
(6, 2, "TO"),
(6, 3, "BCC"),
(7, 2, "TO"),
(8, 2, "TO"),
(8, 3, "BCC"),
(8, 3, "BCC"),
(9, 1, "TO"),
(10, 2, "TO"),
(11, 2, "TO"),
(12, 1, "TO"),
(13, 2, "TO"),
(14, 2, "TO"),
(15, 2, "TO"),
(15, 1, "CC"),
(16, 2, "TO"),
(17, 2, "TO"),
(18, 2, "TO"),
(19, 2, "TO"),
(19, 3, "CC"),
(19, 4, "CC"),
(20, 1, "TO"),
(20, 2, "CC"),
(21, 1, "TO"),
(22, 1, "TO"),
(23, 2, "TO"),
(23, 1, "CC"),
(23, 4, "BCC"),
(24, 1, "TO"),
(25, 1, "TO");


