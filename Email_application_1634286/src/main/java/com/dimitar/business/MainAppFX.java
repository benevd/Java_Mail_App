package com.dimitar.business;

import com.dimitar.email_application_1634286.view.RegisterEmailLayoutController;
import com.dimitar.email_application_1634286.view.RootLayoutController;
import com.dimitar.persistence.DatabaseSeeder;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Create a layout that will contain other containers. Each container is written
 * as standalone programs. Methods used to provide a reference to a container are
 * included.
 * 
 * @author Dimitar
 */
public class MainAppFX extends Application{
    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);
    
    //Views for displaying
    private Stage primaryStage;
    private Stage registrationStage;
    private AnchorPane rootLayout;
    private AnchorPane registrationLayout;
    
    //Locale data
    private Locale locale;
    
    //User credentials
    private Properties credentialProps;
    
    //Other properties
    private Properties dbProps;
    
    //Reference to the RootLayoutController
    private RootLayoutController rootController;
    
    //Reference to the RegisterEmailLayoutController
    private RegisterEmailLayoutController registerController;
    
    //Reference to the path to the properties files
    private Path propsFile;
    private Path dbPropsFile;
    
    /**
     * Default constructor.
     */
    public MainAppFX(){
        super();
    }
    
    /**
     * Main method that launches the application GUI.
     * @param args 
     */
    public static void main(String[] args){
        launch(args);
        System.exit(0);
    }
    
    /**
     * Starts the primary stage (root layout).
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        //Set the locale
        locale = Locale.getDefault();
        LOG.info("COUNTRY ------------ "+locale.getCountry());
        LOG.info("LANGUAGE ----------- "+locale.getLanguage());
        
        this.primaryStage = primaryStage;
        
        //Set the application icon
        Image img = new Image(getClass().getResourceAsStream("/images/app_icon.png"));
        this.primaryStage.getIcons().add(img);
        
        //Seed DB if first time using app
        seedDBFirst();
        
        //Init the root layout which uses DB properties that must be set
        initRootLayout();
        initRegistrationLayout();
        
        
        this.primaryStage.setTitle(ResourceBundle.getBundle("LanguageBundle", locale).getString("Title"));
        
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            verifyEmailRegistration();
            
        LOG.info("Application successfully started");
    }

    /**
     * Initializes the root layout so it is ready for display.
     */
    public void initRootLayout() {
        try{
            //Load root FXML file
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("LanguageBundle", locale));
            loader.setLocation(MainAppFX.class.getResource("/fxml/RootLayout.fxml"));
            rootLayout = (AnchorPane) loader.load();
            rootController = loader.getController(); 
            //Send a reference to this class to the RootLayoutController
            rootController.setMainAppFXReference(this);
        }
        catch(IOException e){
            LOG.error("Failed to find the root fxml", e);
            Platform.exit();
        }
    }
    
    /**
     * Initializes the registration/config layout.
     */
    private void initRegistrationLayout() {
        try {
            //Load email registration form FXML
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("LanguageBundle", locale));
            loader.setLocation(MainAppFX.class.getResource("/fxml/RegisterEmailLayout.fxml"));
            registrationLayout = (AnchorPane) loader.load();
            registerController = loader.getController();
            //Pass a reference to this class to the Register controller
            registerController.setMainAppFX(this);
            
            //Pass a reference to the RegisterEmailLayoutController to RootLayoutController
            rootController.setRegisterControllerToRoot(registerController);
            //Pass a reference to the RootLayoutController to the Register controller
            if(rootController != null){
                registerController.setRootControllerReference(rootController);
            }
            
            registrationStage = new Stage();
            Scene scene = new Scene(registrationLayout);
            registrationStage.setScene(scene);
            registrationStage.initStyle(StageStyle.UTILITY);
            //Set properties
            setCredentialProperties();
            setOtherProperties();
            //Pass a reference to the properties to the Register controller
            registerController.setCredentialsProperties(credentialProps);
            registerController.setDBProperties(dbProps);
            //Pass a reference to the Path of the properties files
            registerController.setPropertiesPath(propsFile);
            registerController.setDBPropertiesPath(dbPropsFile);
        } catch (IOException e) {
            LOG.error("Failed to find the registration layout fxml", e);
            Platform.exit();
        }
    }
    
    /**
     * Utility method used by the RegisterEmailLayoutController to close the
     * registration window after email was successfully registered and show the
     * email application.It also tells the root controller about the current
     * user's credentials so they can be used to send/receive email in the app.
     * @param user
     * @param password
     * @param imapURL
     * @param smtpURL
     * @param isFirstTime
     */
    public void showEmailAppAfterRegistration(String user, String password, String imapURL, String smtpURL, boolean isFirstTime){
        if(isFirstTime){
            registrationStage.hide();
        }
        rootController.setCredentials(user, password, imapURL, smtpURL);
        //Show the main application component
        primaryStage.show();
    }
    
    /**
     * Verify if there is an GMail account registered with the application by
     * accessing the properties. If not, the email registration form is popped up
     * and prompts the user to register their GMail account.
     */
    private void verifyEmailRegistration() { 
        //Check for all properties
        if (credentialProps.containsKey("username") && credentialProps.containsKey("password") && 
                credentialProps.containsKey("imapURL") && credentialProps.containsKey("smtpURL")) {
            //Get the user and password
            String user = credentialProps.getProperty("username");
            String password = credentialProps.getProperty("password");
            String imapURL = credentialProps.getProperty("imapURL");
            String smtpURL = credentialProps.getProperty("smtpURL");
            showEmailAppAfterRegistration(user, password, imapURL, smtpURL, false);
        } 
        else {
            //Open the form window
            showConfigWindow();
        }

    }
    
    /**
     * Set the credential properties object reference.
     */
    private void setCredentialProperties() {
        credentialProps = new Properties();
        propsFile = get("EmailCredentials.properties");
        if (!Files.exists(propsFile)) {
            File newPropsFile = new File(propsFile.toString());
            try {
                newPropsFile.createNewFile();
            } 
            catch (IOException ex) {
                LOG.error("Error during the creation of a new credentials properties file", ex);
                Platform.exit();
            }
        }

        try (InputStream propFileStream = newInputStream(propsFile)) {
            credentialProps.load(propFileStream);
        } catch (IOException e) {
            LOG.error("Loading the properties file has caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Set the properties object reference for the database configuration.
     */
    private void setOtherProperties() {
        dbProps = new Properties();
        dbPropsFile = get("Database.properties");
        if (!Files.exists(dbPropsFile)) {
            File newPropsFile = new File(dbPropsFile.toString());
            try {
                newPropsFile.createNewFile();
            } catch (IOException ex) {
                LOG.error("Error during the creation of a new database properties file", ex);
                Platform.exit();
            }
        }
        try (InputStream propFileStream = newInputStream(dbPropsFile)) {
            dbProps.load(propFileStream);
        } catch (IOException e) {
            LOG.error("Loading the properties file has caused an error", e);
            Platform.exit();
        }

    }
    
    /**
     * Show the registration/config window.
     */
    public void showConfigWindow(){
        registrationStage.show();
    }
    
    /**
     * Hide the registration/config window.
     */
    public void hideConfigWindow(){
        registrationStage.hide();
    }
    
    /**
     * Utility method that seeds the database if the application has been opened
     * for the first time, in other words has no properties file or no properties
     * set.
     */
    private void seedDBFirst(){
        DatabaseSeeder seeder = new DatabaseSeeder();
        Path credPropsFile = get("EmailCredentials.properties");
        Path databasePropsFile = get("Database.properties");
        //If properties files do not exist
        if (!Files.exists(credPropsFile) && !Files.exists(databasePropsFile)) {
            try{
                seeder.seed();
            }
            catch(SQLException | IOException e){
                LOG.error("Error seeding DB", e);
            }
        }
        //If the property file is missing the keys needed
        else if (Files.exists(credPropsFile) && Files.exists(databasePropsFile)) {
            Properties credProps = new Properties();
            try (InputStream propFileStream = newInputStream(credPropsFile)) {
                credProps.load(propFileStream);
            } catch (IOException e) {
                LOG.error("Loading the properties file has caused an error", e);
                Platform.exit();
            }
            Properties databaseProps = new Properties();
            try (InputStream propFileStream = newInputStream(databasePropsFile)) {
                databaseProps.load(propFileStream);
            } catch (IOException e) {
                LOG.error("Loading the properties file has caused an error", e);
                Platform.exit();
            }
            if(!credProps.containsKey("username") && !credProps.containsKey("password") && !credProps.containsKey("smtpURL") && !credProps.containsKey("imapURL")
                    && !databaseProps.containsKey("dbUser") && !databaseProps.containsKey("dbPassword") && !databaseProps.containsKey("dbURL")){
                try {
                    seeder.seed();
                } catch (SQLException | IOException e) {
                    LOG.error("Error seeding DB", e);
                }
            }
        }
    }
}
