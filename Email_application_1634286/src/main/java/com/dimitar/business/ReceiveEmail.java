package com.dimitar.business;

import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.activation.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import javax.mail.Flags;
import jodd.mail.EmailAddress;
import jodd.mail.EmailFilter;

/**
 * The class which has the function to receive email
 *
 * @author Dimitar
 */
public class ReceiveEmail {

    //Initialize logger
    private final static Logger LOG = LoggerFactory.getLogger(ReceiveEmail.class);

    //Initialize required variables
    private String imapServerName;// = "imap.gmail.com";
    private String emailReceive;// = "receive.1634286@gmail.com";
    private String emailReceivePwd;// = "joddmailproject1634286";
    
    /**
     * Utility method used to set the credentials for receiving emails.
     * @param user
     * @param password 
     */
    public void setCredentials(String user, String password){
        this.emailReceive = user;
        this.emailReceivePwd = password;
    }
    
    /**
     * Utility method that sets the URL of the IMAP server
     * @param imapServer 
     */
    public void setImapServer(String imapServer){
        this.imapServerName = imapServer;
    }

    /**
     * The method that will receive new emails that have not been seen by the user 
     * @return the array of email beans containing data from the received emails
     * @throws NullPointerException 
     */
    public EmailData[] receiveEmail() throws NullPointerException {
        if (checkEmail(emailReceive)) {
            ImapServer imapServer = MailServer.create()
                    .host(imapServerName)
                    .ssl(true)
                    .auth(emailReceive, emailReceivePwd)
                    //.debugMode(true)
                    .buildImapMailServer();

            try (ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                LOG.info("Message Count: " + session.getMessageCount());
                //We only want to receive emails that have not been read yet, so we need to mark seen mails
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                //Create an array of Email beans that will be filled and returned at the end of the method
                EmailData[] emailBeans = new EmailData[emails.length];
                int beansCounter = 0;
                LOG.info("\n >>>> ReceivedEmail count = " + emails.length);
                for (ReceivedEmail email : emails) {
                    //Extracting basic email data from the current received email to an email bean
                    EmailData emailBean = receiveBasicData(email);
                    
                    //Extracting the attachments from the current received email to an email bean (if there are any!)
                    List<EmailAttachment<? extends DataSource>> attachmentsList = email.attachments();
                    if(attachmentsList != null){
                        emailBean = receiveAttachments(emailBean, attachmentsList);
                    }
                    //Storing the email bean at the current index in the array
                    emailBeans[beansCounter] = emailBean;
                    beansCounter++;
                }
                return emailBeans;
            }
        } 
        else {
            LOG.info("Unable to receive email because the receive address is invalid");
            return null;
        }
    }
    
    /**
     * Helper method that retrieves the basic data from a ReceivedEmail object
     * @param email
     * @return the email bean with the data from the ReceivedEmail object
     */
    private EmailData receiveBasicData(ReceivedEmail email){
        String from;
        EmailAddress[] to;
        EmailAddress[] CC;
        String subject;
        String textMessage = "";
        String htmlMessage = "";
        LocalDateTime sendTime;
        LocalDateTime receiveTime;
        int messagePriority;
        LOG.info("===[" + email.messageNumber() + "]===");
        // common info
        //FROM
        LOG.info("FROM:" + email.from());
        from = email.from().toString();
        //TO
        LOG.info("TO:" + Arrays.toString(email.to()));
        //Iterate through the recipients array to fill the email bean
        to = email.to();
        //CC
        LOG.info("CC:" + Arrays.toString(email.cc()));
        if (email.cc() != null) {
            CC = email.cc();
        } else {
            CC = null;
        }
        //SUBJECT
        LOG.info("SUBJECT:" + email.subject());
        subject = email.subject();
        //PRIORITY
        LOG.info("PRIORITY:" + email.priority());
        messagePriority = email.priority();
        //SEND DATE
        LOG.info("SENT DATE:" + email.sentDate());
        sendTime = email.sentDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(); //Convert Date to LocalDateTime
        //RECEIVED DATE
        LOG.info("RECEIVED DATE: " + email.receivedDate());
        receiveTime = email.receivedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(); //Convert Date to LocalDateTime
        //MESSAGE
        // process messages
        List<EmailMessage> messages = email.messages();
        for (EmailMessage msg : messages) {
            //If the message is plain text
            if (msg.getMimeType().equalsIgnoreCase("TEXT/PLAIN")) {
                textMessage = msg.getContent();
            } //If the message is HTML 
            else {
                htmlMessage = msg.getContent();
            }
        }
        //Creating an empty EmailBean object and set the basic data fields
        EmailData emailBean = new EmailData();
        emailBean.setFrom(from);
        emailBean.setTo(to);
        emailBean.setCC(CC);
        emailBean.setSubject(subject);
        emailBean.setTextMessage(textMessage);
        emailBean.setHtmlMessage(htmlMessage);
        emailBean.setSendTime(sendTime);
        emailBean.setReceiveTime(receiveTime);
        emailBean.setMessagePriority(messagePriority);
        
        return emailBean;
    }
    
    /**
     * Helper method that adds attachments from the list of attachments in a ReceiveEmail object to an email bean instance
     * @param emailBean
     * @param attachmentsList
     * @return the email bean with the attachment data from the attachment list
     */
    private EmailData receiveAttachments(EmailData emailBean, List<EmailAttachment<? extends DataSource>> attachmentsList){
        //Creating Lists for both regular attachments and embedded to be dynamically filled and converted to regular array later
        List<AttachmentData> attachmentList = new ArrayList();
        List<AttachmentData> embeddedAttachmentList = new ArrayList();
        //ATTACHMENTS
        // There may be multiple arrays so they are stored in an array
        for (EmailAttachment attachment : attachmentsList) {
            if (attachment != null) {
                // Write the file to disk
                // Location hard coded in this example
                attachment.writeToFile(new File("C:\\Temp", attachment.getName()));

                byte[] attachmentContent = attachment.toByteArray();
                AttachmentData attachmentBean = new AttachmentData(attachment.getName(), attachmentContent);
                //If the attachment is embedded
                if (attachment.isEmbedded()) {
                    embeddedAttachmentList.add(attachmentBean);
                } //If it is a regular attachment
                else {
                    attachmentList.add(attachmentBean);
                }
            } else {
                LOG.info("Attachment was not retrieved");
            }
        }
        //Converting the embedded and regular attachments lists to regular arrays to use in email bean
        AttachmentData[] attachments = attachmentList.toArray(new AttachmentData[attachmentList.size()]);
        AttachmentData[] embeddedAttachments = embeddedAttachmentList.toArray(new AttachmentData[embeddedAttachmentList.size()]);
        //Add the attachments to the email bean
        emailBean.setAttachments(attachments);
        emailBean.setEmbeddedAttachments(embeddedAttachments);
        return emailBean;
    }

    /**
     * Use the RFC2822 Address Parser to check if an email might be valid
     *
     * @param address
     * @return true if the email is valid and false if it is not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
}
