package com.dimitar.business;

import jodd.mail.EmailAddress;
import com.dimitar.beans.EmailData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * The class which has the function to send an email
 *
 * @author Dimitar
 */
public class SendEmail {

    private String smtpServerName;// = "smtp.gmail.com";
    
    //Default password
    private String password;// = "joddmailproject1634286";

    //Initialize logger
    private final static Logger LOG = LoggerFactory.getLogger(SendEmail.class);
    
    /**
     * Utility method that sets the password of the user that is connected
     * @param password 
     */
    public void setPassword(String password){
        this.password = password;
    }
    
    /**
     * Utility method that sets the URL of the STMP server
     * @param smtpServer 
     */
    public void setSmtpServer(String smtpServer){
        this.smtpServerName = smtpServer;
    }

    /**
     * This method sends an email based on the bean it receives as input
     *
     * @param email
     */
    public void sendEmail(EmailData email) {
        //Check if email send & receive are valid
        boolean isValidTo = true;
        boolean isValidFrom = true;
        //Go through the array of recipients and check if emails are valid
        if(email.getTo() == null){
            isValidTo = false;
        }
        else{
            for (EmailAddress to : email.getTo()) {
                if (to == null || !checkEmail(to.getEmail()) ) {
                    isValidTo = false;
                }
            }
        }
        if(email.getFrom() == null || !checkEmail(email.getFrom())){
            isValidFrom = false;
        }

        //Check for null in plain text & html fields to avoid application freezing
        if(email.getTextMessage() == null || email.getHtmlMessage() == null){
            throw new IllegalArgumentException("Text content's value cannot be null");
        }
        //If all address fields are valid, proceed to next step
        if (isValidFrom && isValidTo) {
            // Create am SMTP server object
            SmtpServer smtpServer = MailServer.create()
                    .ssl(true)
                    .host(smtpServerName)
                    .auth(email.getFrom(), password)
                    .buildSmtpMailServer();
            
            //Standard email
            if(email.getCC() == null && email.getBCC() == null){
                sendBasicEmail(email, smtpServer);
                LOG.info("Basic email sent");
            }
            //Email with CC
            else if (email.getCC() != null || email.getBCC() != null) {
                sendEmailWithCC(email, smtpServer);
                LOG.info("Email with CC/BCC sent");
            }
            //Invalid email
            else{
                throw new IllegalArgumentException("ERROR: invalid email");
            }
        } 
        //If an address is invalid, then log the error
        else {
            throw new IllegalArgumentException("Unable to send email because either send or one of the receive addresses are invalid");
        }
    }

    /**
     * Send a basic email
     *
     * @param email
     * @param smtpServer
     */
    private void sendBasicEmail(EmailData email, SmtpServer smtpServer) {
        // Using the fluent style of coding create a plain text message
        //Iterate through the list of recipients to send each of them an email
        for (EmailAddress to : email.getTo()) {
            Email joddMail = Email.create().from(email.getFrom())
                    .to(to.getEmail())
                    .subject(email.getSubject())
                    .textMessage(email.getTextMessage())
                    .htmlMessage(email.getHtmlMessage());
            
            //If the email bean contains any attachment(s), add them to the current jodd email object
            if(email.getEmbeddedAttachments() != null || email.getAttachments() != null){
                joddMail = addAttachmentToMail(email, joddMail);
            }
            
            // Like a file we open the session, send the message and close the
            // session
            try ( // A session is the object responsible for communicating with the server
                    SendMailSession session = smtpServer.createSession()) {
                // Like a file we open the session, send the message and close the
                // session
                session.open();
                session.sendMail(joddMail);
                LOG.info("Email to " + to + " sent");
            }
        }
    }

    /**
     * Send an email with CC/BCC
     *
     * @param email
     * @param smtpServer
     */
    private void sendEmailWithCC(EmailData email, SmtpServer smtpServer) {
        //Check the emails in the CC field
        boolean hasCC = true;
        if(email.getCC() == null){
            hasCC = false;
        }
        else{
            for (EmailAddress cc : email.getCC()) {
                if (!checkEmail(cc.getEmail())) {
                    LOG.info("CC email adress "+cc+" is invalid");
                    throw new IllegalArgumentException("Email "+cc+" in the CC field is invalid - cannot send message");
                }
            }
        }
        
        //Check the emails in the BCC field
        boolean hasBCC = true;
        if(email.getBCC() == null){
            hasBCC = false;
        }
        else{
            for (EmailAddress bcc : email.getBCC()) {
                if (!checkEmail(bcc.getEmail())) {
                    LOG.info("BCC email adress "+bcc+" is invalid");
                    throw new IllegalArgumentException("Email "+bcc+" in the BCC field is invalid - cannot send message");
                }
            }
        }
        //Iterate through the list of recipients to send the email
        for (EmailAddress to : email.getTo()) {
            //Creating Email object using the fluent style requires EmailMessage
            Email joddMail = Email.create().from(email.getFrom())
                        .to(to.getEmail())
                        .subject(email.getSubject())
                        .textMessage(email.getTextMessage())
                        .htmlMessage(email.getHtmlMessage());        
            
            //Attach CC/BCC fields to the Email object
            if(hasCC){
                joddMail.cc(email.getCC());
                //If the email ALSO contains valid BCC adresses
                if(hasBCC){
                    joddMail.bcc(email.getBCC());
                }
            }
            //If the email contains valid BCC adresses
            else{
                joddMail.bcc(email.getBCC());
            }

            //If the email bean contains any attachment(s), add them to the current jodd email object
            if(email.getEmbeddedAttachments() != null || email.getAttachments() != null){
                joddMail = addAttachmentToMail(email, joddMail);
            }
            //Send the email if no problems are encountered so far
            // Like a file we open the session, send the message and close the session
            try ( // A session is the object responsible for communicating with the server
                    SendMailSession session = smtpServer.createSession()) {
                session.open();
                session.sendMail(joddMail);
                LOG.info("Email to " + to + " sent");
            }
        }
        
    }

    /**
     * Helper method for sendBasicEmail and sendEmailWithCC that adds attachments to the jodd email object if the email bean
     * contains either embedded or regular attachments
     *
     * @param email
     * @param smtpServer
     * 
     * @return the jodd Email object with all the embedded and regular attachments contained in the email bean
     */
    private Email addAttachmentToMail(EmailData email, Email joddMail) {
        //If email has embedded attachment(s)    
        if (email.getEmbeddedAttachments() != null) { 
            //Adding all the email bean embedded attachments to the jodd Email object instance
            for (int i = 0; i < email.getEmbeddedAttachments().length; i++) {
                joddMail.embeddedAttachment(EmailAttachment.with()
                        .content(email.getEmbeddedAttachments()[i].getFilePath()));
            }
        }
        //If email has regular attachment(s)
        if (email.getAttachments() != null) {
            //Adding all the email bean regular attachments to the jodd Email object instance
            for (int i = 0; i < email.getAttachments().length; i++) {
                joddMail.attachment(EmailAttachment.with()
                        .content(email.getAttachments()[i].getFilePath()));
            }
        }
        return joddMail;
    }

    /**
     * Use the RFC2822 Address Parser to check if an email might be valid
     *
     * @param address
     * @return true if the email is valid and false if it is not
     */
    public boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
    /**
     * Utility method used to verify if credentials for a GMail account are valid.
     * @param user
     * @param pwd
     * @param smtpServ
     * @return true if the user can sign in, false if the user cannot
     */
    public boolean tryConnecting(String user, String pwd, String smtpServ) {
        setSmtpServer(smtpServ);
        boolean canSignIn = true;
        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtpServerName)
                .auth(user, pwd)
                .buildSmtpMailServer();
        try {
            //Send a sample email
            String from = user;
            String to = "feafadafds33ddeafe@gmail.com";
            EmailAddress[] toArr = new EmailAddress[1];
            EmailAddress recip = new EmailAddress("test", to);
            toArr[0] = recip;
            String textMessage = "This is plain text message";
            EmailData emailBean = new EmailData(from, toArr, null, "", textMessage, "", null, null, null, null, 0);
            sendBasicEmail(emailBean, smtpServer);
        }
        catch(Exception e){
            canSignIn = false;
        }
        finally{
            return canSignIn;
        }
    }
}
