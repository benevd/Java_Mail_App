package com.dimitar.persistence;

import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the AttachmentDAO interface methods for CRUD operations on the
 * Attachment table in the Email database
 * @author Dimitar
 */
public class AttachmentDAOImpl implements AttachmentDAO{
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentDAOImpl.class);
    //CREATE
    /**
     * Creates an attachment records in the database from supplied information
     * @param emailID
     * @param name
     * @param content
     * @param type
     * @param connection
     * @return the number of attachment records inserted (should always be 1)
     * @throws SQLException 
     */
    @Override
    public int createAttachment(int emailID, String name, byte[] content, String type, Connection connection) throws SQLException {
        //Check for valid attachment type
        if(!type.equalsIgnoreCase("regular") && !type.equalsIgnoreCase("embedded")){
            throw new IllegalArgumentException("Attachment type "+type+" is invalid");
        }
        int result;
        String createAttachmentQuery = "INSERT INTO attachment (file_name, content, attach_type, email_id) VALUES (?, ?, ?, ?)";
        try(PreparedStatement createAttachmentStmnt = connection.prepareStatement(createAttachmentQuery, Statement.RETURN_GENERATED_KEYS);){
            createAttachmentStmnt.setString(1, name);
            createAttachmentStmnt.setBytes(2, content);
            createAttachmentStmnt.setString(3, type);
            createAttachmentStmnt.setInt(4, emailID);
            
            result = createAttachmentStmnt.executeUpdate();
        }
        LOG.info(result+" attachment records have been inserted");
        return result;
    }
    
}
