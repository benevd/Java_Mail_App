package com.dimitar.persistence;

import java.sql.*;

/**
 * Interface for CRUD methods used on the Email_address bridging table in the Email
 * database
 * 
 * @author Dimitar
 */
public interface EmailAddressDAO {
    //CREATE
    public int createEmailAddressEntry(int emailID, String emailAddr, String type, Connection connection) throws SQLException;
}
