package com.dimitar.persistence;

import java.sql.*;
import java.util.List;

/**
 * Interface for CRUD methods used on the Folder table in the Email database
 * 
 * @author Dimitar
 */
public interface FolderDAO {
    //CREATE
    public int createFolder(String name, Connection connection) throws SQLException;
    
    //READ
    public List<String> findAllFolders(Connection connection) throws SQLException;
    
    //UPDATE
    public int renameFolder(String currentName,String newName, Connection connection) throws SQLException;
    
    //DELETE
    public int deleteFolder(String name, Connection connection) throws SQLException;
}
