package com.dimitar.persistence;

import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.sql.SQLException;
import java.util.List;
import java.sql.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.nio.file.Files.newInputStream;
import javafx.application.Platform;
import jodd.mail.EmailAddress;

/**
 * This class implements the EmailDAO interface and its features for CRUD operations
 * in the Email application database
 * 
 * @author Dimitar
 */
public class EmailDAOImpl implements EmailDAO{
    
    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOImpl.class);
    
    private String url = "";
    private String user = "";
    private String password = "";
    
    //References to helper DAO classes
    private final AddressDAO addressDAO = new AddressDAOImpl();
    private final FolderDAO folderDAO = new FolderDAOImpl();
    private final EmailAddressDAO emailAddressDAO = new EmailAddressDAOImpl();
    private final AttachmentDAO attachmentDAO = new AttachmentDAOImpl();
    
    /**
     * Helper method that makes the connection to the database using user credentials
     * from a properties file
     * @return the connection object
     * @throws IOException
     * @throws SQLException 
     */
    private void setConnectionInfo() throws IOException {
        //Getting connection information from property file
        Properties props = new Properties();
        Path propsFile = get("Database.properties");
        if (!Files.exists(propsFile)) {
            File newPropsFile = new File(propsFile.toString());
            newPropsFile.createNewFile();
        }
        try (InputStream propFileStream = newInputStream(propsFile)) {
            props.load(propFileStream);
        }
        if (props.containsKey("dbUser") && props.containsKey("dbPassword") && props.containsKey("dbURL")) {
            user = props.getProperty("dbUser");
            password = props.getProperty("dbPassword");
            url = props.getProperty("dbURL");
        } //If there are no properties yet set placeholder values to be able to init the root controller
        else {
            user = "dimitar";
            password = "dimitar";
            url = "jdbc:mysql://localhost:3306/EMAIL?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        }
    }
    
    //CREATE
    /**
     * This method adds a folder record to the database from the name provided as argument
     * 
     * @param name
     * @return the number of folder records created (should be 1)
     * @throws SQLException
     * @throws java.io.IOException
     */
    @Override
    public int createFolder(String name) throws SQLException, IOException{
        int result;
        setConnectionInfo();
        try (Connection connection = DriverManager.getConnection(url, user, password);){
            result = folderDAO.createFolder(name, connection);
        }
        return result;
    }

    /**
     * This method creates and adds an email record to the database from the extracted data from the email bean
     * supplied as argument
     * @param emailBean
     * @return the number of email records created (should be 1)
     * @throws SQLException 
     * @throws java.io.IOException 
     */
    @Override
    public int create(EmailData emailBean) throws SQLException, IOException {
        int result;
        int folderID;
        int emailID;
        String findFolderQuery = "SELECT folder_id FROM folder WHERE name=?";
        String createEmailQuery = "INSERT INTO email (subject, html_msg, text_msg, sent_time, received_time, msg_priority, folder_id, sender) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        setConnectionInfo();
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement getFolder = connection.prepareStatement(findFolderQuery);
                PreparedStatement createEmail = connection.prepareStatement(createEmailQuery, Statement.RETURN_GENERATED_KEYS);
                Statement stmnt = connection.createStatement();){
            //Find the folder in which the emailBean should be placed in the database
            getFolder.setString(1, emailBean.getFolderName());
            ResultSet folderResult = getFolder.executeQuery();
            folderResult.next();
            folderID = folderResult.getInt("folder_id");
            
            //Inserting emailBean data inside the Email table
            createEmail.setString(1, emailBean.getSubject());
            createEmail.setString(2, emailBean.getHtmlMessage());
            createEmail.setString(3, emailBean.getTextMessage());
            createEmail.setTimestamp(4, Timestamp.valueOf(emailBean.getSendTime()));
            if(emailBean.getReceiveTime() == null){
                createEmail.setTimestamp(5, null);
            }
            else{
                createEmail.setTimestamp(5, Timestamp.valueOf(emailBean.getReceiveTime()));
            }
            createEmail.setInt(6, emailBean.getMessagePriority());
            createEmail.setInt(7, folderID);
            createEmail.setString(8, emailBean.getFrom());
            result = createEmail.executeUpdate();
            
            //Inserting emailBean data inside the Email_address bridging table
            //Get the id from the Email record that was just inserted
            ResultSet emailIDResult = stmnt.executeQuery("SELECT MAX(email_id) FROM email");
            emailIDResult.next();
            emailID = emailIDResult.getInt(1);
            //Adding each recipient's email address to the bridging table
            for(EmailAddress emailAddr : emailBean.getTo()){
                //Add the email address to the address table if it is not already there
                addressDAO.createAddress(emailAddr.getEmail(), emailAddr.getPersonalName(), connection);
                //Insert the recipient record in the bridging table
                emailAddressDAO.createEmailAddressEntry(emailID, emailAddr.getEmail(), "TO", connection);
            }
            //If the email bean has CC email addresses, go through the array and add them to the bridging table
            if (emailBean.getCC() != null) {
                for (EmailAddress emailAddr : emailBean.getCC()) {
                    //Add the email address to the address table if it is not already there
                    addressDAO.createAddress(emailAddr.getEmail(), emailAddr.getPersonalName(), connection);
                    //Insert the CC record in the bridging table
                    emailAddressDAO.createEmailAddressEntry(emailID, emailAddr.getEmail(), "CC", connection);
                }
            }
            //If the email bean has BCC email addresses, go through the array and add them to the briding table
            if(emailBean.getBCC() != null){
                for(EmailAddress emailAddr : emailBean.getBCC()){
                    //Add the email address to the address table if it is not already there
                    addressDAO.createAddress(emailAddr.getEmail(), emailAddr.getPersonalName(), connection);
                    //Insert the BCC record in the bridging table
                    emailAddressDAO.createEmailAddressEntry(emailID, emailAddr.getEmail(), "BCC", connection);
                }
            }
            //If the email bean has regular attachments, go through the array and add them to the attachment table
            if(emailBean.getAttachments() != null){
                for(AttachmentData attachment : emailBean.getAttachments()){
                    attachmentDAO.createAttachment(emailID, attachment.getName(), attachment.getAttachment(), "regular", connection);
                }
            }
            //If the email bean has embedded attachments, go through the array and add them to the attachment table
            if(emailBean.getEmbeddedAttachments() != null){
                for(AttachmentData attachment : emailBean.getEmbeddedAttachments()){
                    attachmentDAO.createAttachment(emailID, attachment.getName(), attachment.getAttachment(), "embedded", connection);
                }
            }
        }
        LOG.info("Number of email records inserted: "+result);
        return result;            
    }
    
    //READ
    /**
     * Finds all emails and their associated records, extracts their data to EmailData 
     * objects and place them in a list
     * 
     * @return the list of all EmailData beans stored in the database
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public List<EmailData> findAll() throws SQLException, IOException {
        List<EmailData> emailRecords;
        emailRecords = findEmailByCriteria("");
        return emailRecords;
    }
    
    /**
     * Finds an email with a specific ID and extracts its data to an EmailData object
     * 
     * @param id
     * @return the email from the database with the specific id passed as argument
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public EmailData findID(int id) throws SQLException, IOException {
        String idCriteria = "email_id = " + id;
        List<EmailData> emailRecords = findEmailByCriteria(idCriteria);
        if(emailRecords.isEmpty()){
            return null;
        }
        EmailData emailRecord = emailRecords.get(0);
        return emailRecord;
    }
    
    /**
     * Finds all emails that belong to a specific folder, extracts their data to an EmailData object
     * and adds them to the list
     * 
     * @param folderName
     * @return the list of emails from the database that belong to the folder passed as argument
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public List<EmailData> findFolder(String folderName) throws SQLException, IOException {
        String folderCriteria = "f.name = '" + folderName+"'";
        List<EmailData> emailRecords = findEmailByCriteria(folderCriteria);
        return emailRecords;
    }
    
    /**
     * Finds all emails based on their subject, extracts their data to an EmailData object
     * and adds them to the list
     * @param subject
     * @return the list of emails from the database that fit the subject criteria passed as argument
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public List<EmailData> findSubject(String subject) throws SQLException, IOException {
        String subjectCriteria;
        if(subject.isEmpty()){
            subjectCriteria = "e.subject = ''";
        }
        else{
            subjectCriteria = "e.subject LIKE '%"+subject+"%'";
        }
        List<EmailData> emailRecords = findEmailByCriteria(subjectCriteria);
        return emailRecords;
    }
    
    /**
     * Finds all folders in the database
     * @return a list that contains all the folder names
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public List<String> findAllFolders() throws SQLException, IOException{
        List<String> folders;
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);){
            folders = folderDAO.findAllFolders(connection);
        }
        return folders;
    }

    //UPDATE
    /**
     * Updates the folder to which a specific email belongs to
     * 
     * @param ID
     * @param newFolderName
     * @return the number of email records that have been update (should always be 1)
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public int updateFolder(int ID, String newFolderName) throws SQLException, IOException {
        int result;
        String getFolderIDQuery = "SELECT folder_id FROM folder WHERE name = ?";
        String updateFolderQuery = "UPDATE email SET folder_id = ? WHERE email_id = ?";
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement getFolderID = connection.prepareStatement(getFolderIDQuery);
                PreparedStatement updateFolder = connection.prepareStatement(updateFolderQuery);){
           getFolderID.setString(1, newFolderName);
           //Getting the appropriate folderID
           ResultSet getFolderIDResult = getFolderID.executeQuery();
           getFolderIDResult.next();
           int folderID = getFolderIDResult.getInt("folder_id");
           updateFolder.setInt(1, folderID);
           updateFolder.setInt(2, ID);
           result = updateFolder.executeUpdate();
        }
        LOG.info(result+" email records have been updated");
        return result;
    }
    
    /**
     * Renames a folder
     * @param currentName
     * @param newName
     * @return the number of folders that have been renamed (should always be 1)
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public int renameFolder(String currentName, String newName) throws SQLException, IOException{
        int result;
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);){
            result = folderDAO.renameFolder(currentName, newName, connection);
        }
        return result;
    }

    //DELETE
    /**
     * Deletes an email and all of its associated records from the database
     * 
     * @param ID
     * @return the number of email records that have been deleted (should always be 1)
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public int delete(int ID) throws SQLException, IOException {
        int result;
        String deleteEmailQuery = "DELETE FROM email WHERE email_id = ?";
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement deleteEmail = connection.prepareStatement(deleteEmailQuery);){
            deleteEmail.setInt(1, ID);
            result = deleteEmail.executeUpdate();
        }
        LOG.info(result+" emails have been deleted");
        return result;
    }

    /**
     * Deletes a folder and all of its associated records from the database
     * @param name
     * @return the number of folder records that have been deleted (should always be 1)
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public int deleteFolder(String name) throws SQLException, IOException {
        int result;
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);){
            result = folderDAO.deleteFolder(name, connection);
        }
        return result;
    }
    
    //PRIVATE CRUD HELPER METHODS
    /**
     * Helper methods that holds the functionality that will be shared among all 
     * read DAO methods to find any email records based on a specific criteria and
     * extract their data to EmailData objects placed in a list
     * 
     * @param criteria
     * @return the list of email beans created from found email records based on the given criteria
     * @throws SQLException
     * @throws IOException 
     */
    private List<EmailData> findEmailByCriteria(String criteria) throws SQLException, IOException{
        int currentID;
        List<EmailData> emailRecords;
        List<Integer> emailIds;
        String getAllEmailsQuery;
        String getAllEmailIdsQuery;
        //If there is no specified critera, find all emails
        if(criteria.isEmpty()){
            getAllEmailsQuery = "SELECT email_id, subject, html_msg, text_msg, sent_time, received_time, msg_priority, sender, f.name"
                + " FROM email e JOIN folder f ON f.folder_id=e.folder_id ORDER BY e.email_id ASC";
            getAllEmailIdsQuery = "SELECT email_id FROM email e JOIN folder f ON f.folder_id=e.folder_id ORDER BY e.email_id ASC";
        }
        //If there is a specific criteria, apply it to the query in the WHERE clause
        else{
            getAllEmailsQuery = "SELECT email_id, subject, html_msg, text_msg, sent_time, received_time, msg_priority, sender, f.name"
                + " FROM email e JOIN folder f ON f.folder_id=e.folder_id WHERE "+criteria+" ORDER BY e.email_id ASC";
            getAllEmailIdsQuery = "SELECT email_id FROM email e JOIN folder f ON f.folder_id=e.folder_id WHERE "+criteria+" ORDER BY e.email_id ASC";
        }
        
        String getAllTOCCBCCQuery = "SELECT email_addr, type, personal_name FROM address a "
                        + "JOIN email_address ea ON a.address_id=ea.address_id WHERE ea.email_id = ?";
        String getAllAttachmentsQuery = "SELECT file_name, content, attach_type FROM attachment WHERE email_id= ?";
        setConnectionInfo();
        try(Connection connection = DriverManager.getConnection(url, user, password);
                Statement getAllEmails = connection.createStatement();
                Statement getAllEmailIds = connection.createStatement();
                PreparedStatement getAllTOCCBCC = connection.prepareStatement(getAllTOCCBCCQuery);
                PreparedStatement getAllAttachments = connection.prepareStatement(getAllAttachmentsQuery);){
            //Creating email beans from the ResultSet and getting all of their IDs
            ResultSet retrievedEmails = getAllEmails.executeQuery(getAllEmailsQuery);
            ResultSet retrievedIds = getAllEmailIds.executeQuery(getAllEmailIdsQuery);
            emailIds = getEmailIds(retrievedIds);
            emailRecords = createEmailData(retrievedEmails);
            
            //Going through the list of emailBeans to add recipients (and CC, BCC & attachments if there are any)
            for(int i=0; i<emailRecords.size(); i++){
                //Getting the ID of the current email we are attaching additional data to
                currentID = emailIds.get(i);
                //Getting all the email addresses from the bridging table that belong to the current email instance
                getAllTOCCBCC.setInt(1, currentID);
                ResultSet emailAddresses = getAllTOCCBCC.executeQuery();
                EmailData emailBeanWithTOCCBCC = addTOCCBCC(emailRecords.get(i), emailAddresses);
                emailRecords.set(i, emailBeanWithTOCCBCC);
                
                //Adding attachments (only if there are any)
                getAllAttachments.setInt(1, currentID);
                ResultSet attachments = getAllAttachments.executeQuery();
                EmailData emailBeanWithAttachments = addAttachments(emailRecords.get(i), attachments);
                if(emailBeanWithAttachments != null){
                    emailRecords.set(i, emailBeanWithAttachments);
                }
            }
        }
        return emailRecords;
    }
    
    /**
     * Helper method that adds TO, CC and BCC from a ResultSet to an existing
     * email bean that will be replaced with the returned bean
     * 
     * @param emailBean
     * @param emailAddresses
     * @return the email bean with added TO, CC and BCC
     * @throws SQLException 
     */
    private EmailData addTOCCBCC(EmailData emailBean, ResultSet emailAddresses) throws SQLException{
        //Creating 3 String lists to hold each type of email addresses
        List<EmailAddress> to = new ArrayList<>();
        List<EmailAddress> cc = new ArrayList<>();
        List<EmailAddress> bcc = new ArrayList<>();
        String email;
        String type;
        String personalName;
        EmailAddress emailAddress;
        //Filling the lists with the appropriate email addresses
        while (emailAddresses.next()) {
            //Get current email address
            email = emailAddresses.getString("email_addr");
            //Get the type of the current email address record
            type = emailAddresses.getString("type");
            //Get the personal name of the current email address record
            personalName = emailAddresses.getString("personal_name");
            //Initialize the Jodd EmailAddress from the information gathered
            emailAddress = new EmailAddress(personalName, email);
            //If email is a recipient (TO)
            if (type.equalsIgnoreCase("to")) {
                to.add(emailAddress);
            } //If email is a CC
            else if (type.equalsIgnoreCase("cc")) {
                cc.add(emailAddress);
            } //If email is a BCC
            else if (type.equalsIgnoreCase("bcc")) {
                bcc.add(emailAddress);
            } //If not of any type, error
            else {
                throw new IllegalArgumentException("An email address is not of any valid type");
            }
        }
        //Convert each of the lists back to arrays to assign them to the emailBean
        EmailAddress[] recipients = to.toArray(new EmailAddress[to.size()]);
        emailBean.setTo(recipients);
        //If there were any CCs
        if (cc.size() > 0) {
            EmailAddress[] CC = cc.toArray(new EmailAddress[cc.size()]);
            emailBean.setCC(CC);
        }
        //If there were any BCCs
        if (bcc.size() > 0) {
            EmailAddress[] BCC = bcc.toArray(new EmailAddress[bcc.size()]);
            emailBean.setBCC(BCC);
        }
        return emailBean;
    }
    
    /**
     * Helper method that adds attachments from a ResultSet to an existing email
     * bean that will be replaced by the returned bean
     * 
     * @param emailBean
     * @param attachments
     * @return the email bean with attachments (null if no attachments have been added)
     * @throws SQLException 
     */
    private EmailData addAttachments(EmailData emailBean, ResultSet attachments) throws SQLException{
        //Creating 2 AttachmentData lists to hold each type of attachment
        List<AttachmentData> regular = new ArrayList<>();
        List<AttachmentData> embedded = new ArrayList<>();
        String attachType;
        while (attachments.next()) {
            //Get the file name and contents to create an attachment bean
            String fileName = attachments.getString("file_name");
            byte[] contents = attachments.getBytes("content");
            AttachmentData attachment = new AttachmentData(fileName, contents);
            //Get the type of attachment
            attachType = attachments.getString("attach_type");
            //If embedded attachment
            if (attachType.equalsIgnoreCase("embedded")) {
                embedded.add(attachment);
            } //If regular attachment
            else if (attachType.equalsIgnoreCase("regular")) {
                regular.add(attachment);
            } //If it does not fit any type, error
            else {
                throw new IllegalArgumentException("An attachment's type is invalid");
            }
        }
        //Converting each attachment list to arrays so they can be added to the emailBean
        //If there are any attachments
        if(embedded.size() > 0 || regular.size() > 0){
            //If there were any embedded attachments
            if (embedded.size() > 0) {
                AttachmentData[] embeddedArr = embedded.toArray(new AttachmentData[embedded.size()]);
                emailBean.setEmbeddedAttachments(embeddedArr);
            }
            //If there were any regular attachments
            if (regular.size() > 0) {
                AttachmentData[] regularArr = regular.toArray(new AttachmentData[regular.size()]);
                emailBean.setAttachments(regularArr);
            }
        }
        //If there are no attachments return null so the email bean reference is not replaced in the parent method
        else{
            return null;
        }
        return emailBean;
    }
    
    /**
     * Helper method that creates a bean with basic singular data from a ResultSet
     * 
     * @param resultSet
     * @return the email bean with basic data added to it
     * @throws SQLException 
     */
    private List<EmailData> createEmailData(ResultSet resultSet) throws SQLException{
        List<EmailData> emailBeansBasic = new ArrayList<>();
        while(resultSet.next()){
            EmailData emailBean = new EmailData();
            emailBean.setID(resultSet.getInt("email_id"));
            emailBean.setFrom(resultSet.getString("sender"));
            emailBean.setSendTime(resultSet.getTimestamp("sent_time").toLocalDateTime());
            Timestamp receiveTime = resultSet.getTimestamp("received_time");
            if(receiveTime == null){
                emailBean.setReceiveTime(null);
            }
            else{
                emailBean.setReceiveTime(receiveTime.toLocalDateTime());
            }
            emailBean.setFolderName(resultSet.getString(9));
            emailBean.setHtmlMessage(resultSet.getString("html_msg"));
            emailBean.setTextMessage(resultSet.getString("text_msg"));
            emailBean.setMessagePriority(resultSet.getInt("msg_priority"));
            emailBean.setSubject(resultSet.getString("subject"));
            emailBeansBasic.add(emailBean);
        }
        return emailBeansBasic;
    }
    
    private List<Integer> getEmailIds (ResultSet resultSet) throws SQLException{
        List<Integer> emailIds = new ArrayList<>();
        while(resultSet.next()){
            int id = resultSet.getInt("email_id");
            emailIds.add(id);
        }
        return emailIds;
    }
}
