package com.dimitar.persistence;

import com.dimitar.beans.EmailData;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods used in the email application
 * @author Dimitar
 */
public interface EmailDAO {
    //CREATE
    public int createFolder(String name) throws SQLException, IOException;
    
    public int create(EmailData emailBean) throws SQLException, IOException;
    
    //READ
    public List<EmailData> findAll() throws SQLException, IOException;
    
    public EmailData findID(int id) throws SQLException, IOException;
    
    public List<EmailData> findFolder(String folderName) throws SQLException, IOException;
    
    public List<EmailData> findSubject(String subject) throws SQLException, IOException;
    
    public List<String> findAllFolders() throws SQLException, IOException;
    
    //UPDATE
    public int updateFolder(int ID, String newFolderName) throws SQLException, IOException;
    
    public int renameFolder(String currentName, String newName) throws SQLException, IOException;
    
    //DELETE
    public int delete(int ID) throws SQLException, IOException;
    
    public int deleteFolder(String name) throws SQLException, IOException;
}
