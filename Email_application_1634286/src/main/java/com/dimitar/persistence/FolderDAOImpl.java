package com.dimitar.persistence;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the FolderDAO interface methods for CRUD operations on the
 * Folder table in the Email database
 * 
 * @author Dimitar
 */
public class FolderDAOImpl implements FolderDAO{
    private final static Logger LOG = LoggerFactory.getLogger(FolderDAOImpl.class);
    //CREATE
    /**
     * This method adds a folder record to the database from the name provided as argument
     * 
     * @param name
     * @param connection
     * @return the number of folder records created (should be 1)
     * @throws SQLException
     */
    @Override
    public int createFolder(String name, Connection connection) throws SQLException {
        int result;
        String query = "INSERT INTO folder (name) VALUES (?)";
        try (PreparedStatement stmnt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)){
            stmnt.setString(1, name);
            
            result = stmnt.executeUpdate();
        }
        LOG.info(result+" folder records have been created");
        return result;
    }
    
    //READ
    /**
     * Finds all folders in the database
     * 
     * @param connection
     * @return a list that contains all the folder names
     * @throws SQLException 
     */
    @Override
    public List<String> findAllFolders(Connection connection) throws SQLException {
        String findAllFoldersQuery = "SELECT name FROM folder ORDER BY folder_id";
        List<String> folders = new ArrayList<>();
        try(Statement findAllFoldersStmnt = connection.createStatement()){
            ResultSet foldersResult = findAllFoldersStmnt.executeQuery(findAllFoldersQuery);
            while(foldersResult.next()){
                folders.add(foldersResult.getString("name"));
            }
        }
        return folders;
    }

    //UPDATE
    /**
     * Renames a folder
     * @param currentName
     * @param newName
     * @param connection
     * @return the number of folders that have been renamed (should always be 1)
     * @throws SQLException 
     */
    @Override
    public int renameFolder(String currentName, String newName, Connection connection) throws SQLException {
        //Checking if the folder is sent or received emails -> cannot rename those
        if(currentName.equalsIgnoreCase("sent") || currentName.equalsIgnoreCase("received") || currentName.equalsIgnoreCase(newName)){
            throw new IllegalArgumentException("Cannot rename the received or sent or the same folder");
        }
        String updateFolderNameQuery = "UPDATE folder SET name = ? WHERE name=?";
        int result;
        try(PreparedStatement updateFolderNameStmnt = connection.prepareStatement(updateFolderNameQuery, Statement.RETURN_GENERATED_KEYS);){
            updateFolderNameStmnt.setString(1, newName);
            updateFolderNameStmnt.setString(2, currentName);
            result = updateFolderNameStmnt.executeUpdate();
        }
        LOG.info(result+" folders have been renamed");
        return result;
    }

    //DELETE
    /**
     * Deletes a folder and all of its associated records from the database
     * @param name
     * @param connection
     * @return the number of folder records that have been deleted (should always be 1)
     * @throws SQLException 
     */
    @Override
    public int deleteFolder(String name, Connection connection) throws SQLException {
        //Checking if the folder is sent or received emails -> cannot delete those
        if(name.equalsIgnoreCase("sent") || name.equalsIgnoreCase("received")){
            throw new IllegalArgumentException("Cannot delete the received or sent folder");
        }
        int result;
        String deleteFolderQuery = "DELETE FROM folder WHERE name = ?";
        try(PreparedStatement deleteFolder = connection.prepareStatement(deleteFolderQuery, Statement.RETURN_GENERATED_KEYS);){
            deleteFolder.setString(1, name);
            result = deleteFolder.executeUpdate();
        }
        LOG.info(result+" folders have been deleted");
        return result;
    }
    
}
