package com.dimitar.persistence;

import java.sql.SQLException;
import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the AddressDAO interface methods for CRUD operations on the
 * Address table in the Email database
 * 
 * @author Dimitar
 */
public class AddressDAOImpl implements AddressDAO{
    private final static Logger LOG = LoggerFactory.getLogger(AddressDAOImpl.class);
    //CREATE
    /**
     * Create an address record in the database
     * @param emailAddr
     * @param connection
     * @return the number of address records inserted (should always be 1 if the 
     * address does not already exist in the table or 0 if it doesn't)
     * @throws SQLException 
     */
    @Override
    public int createAddress(String emailAddr, String personalName, Connection connection) throws SQLException {
        int result;
        int count;
        String createAddressQuery = "INSERT INTO address (email_addr, personal_name) VALUES (?, ?)";
        try (PreparedStatement createAddressStmnt = connection.prepareStatement(createAddressQuery, Statement.RETURN_GENERATED_KEYS);) {
            //Checking if the email address is already stored in the address table
            count = findCountOfAddress(emailAddr, connection);
            //If there is no matching email address in the address table, add it
            if (count < 1) {
                createAddressStmnt.setString(1, emailAddr);
                createAddressStmnt.setString(2, personalName);
                result = createAddressStmnt.executeUpdate();
            } else {
                result = 0;
            }
        }
        LOG.info("Number of address records inserted:"+result);
        return result;
    }
    //READ
    /**
     * Method used to check if a relevant address record already exists in the database
     * @param emailAddr
     * @param connection
     * @return the number of matching address records
     * @throws SQLException 
     */
    @Override
    public int findCountOfAddress(String emailAddr, Connection connection) throws SQLException {
        int result;
        String findCountOfAddrQuery = "SELECT COUNT(email_addr) FROM address WHERE email_addr = ?";
        try(PreparedStatement findCountOfAddrStmnt = connection.prepareStatement(findCountOfAddrQuery);){
            findCountOfAddrStmnt.setString(1, emailAddr);
            ResultSet countOfAddrResult = findCountOfAddrStmnt.executeQuery();
            countOfAddrResult.next();
            result = countOfAddrResult.getInt(1);
        }
        return result;
    }
    
    /**
     * Find the address_id of a particular email address inside the address table
     * @param emailAddr
     * @param connection
     * @return the address_id of the matching address record
     * @throws SQLException 
     */
    @Override
    public int findIDOfAddress(String emailAddr, Connection connection) throws SQLException{
        int id;
        String findIDOfAddressQuery = "SELECT address_id FROM address WHERE email_addr = ?";
        try(PreparedStatement findIDOfAddressStmnt = connection.prepareStatement(findIDOfAddressQuery);){
            findIDOfAddressStmnt.setString(1, emailAddr);
            ResultSet findIDOfAddressResult = findIDOfAddressStmnt.executeQuery();
            findIDOfAddressResult.next();
            id = findIDOfAddressResult.getInt("address_id");
        }
        return id;
    }
    
}
