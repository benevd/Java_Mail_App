package com.dimitar.persistence;

import java.sql.*;

/**
 * Interface for CRUD methods used on the Attachment table in the Email database
 * 
 * @author Dimitar
 */
public interface AttachmentDAO {
    //CREATE
    public int createAttachment(int emailID, String name, byte[] content, String type, Connection connection) throws SQLException;
}
