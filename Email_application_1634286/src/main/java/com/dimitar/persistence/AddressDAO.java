package com.dimitar.persistence;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface for CRUD methods used on the Address table in the Email database
 * 
 * @author Dimitar
 */
public interface AddressDAO {
    //CREATE
    public int createAddress(String emailAddr, String personalName, Connection connection) throws SQLException;
    
    //READ
    public int findCountOfAddress(String emailAddr, Connection connection) throws SQLException;
    
    public int findIDOfAddress(String emailAddr, Connection connection) throws SQLException;
}
