package com.dimitar.persistence;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.imageio.ImageIO;

/**
 * This class contains all necessary methods to seed the database with sample
 * data. It is used in the EmailDAOTest class and in the RegisterEmailLayoutController
 * class to create sample data when a new user is registered with the email
 * application.
 * 
 * @author Dimitar
 */
public class DatabaseSeeder {
    private final static String URL = "jdbc:mysql://localhost:3306/EMAIL?zeroDateTimeBehavior=CONVERT_TO_NULL";
    private final static String USER = "dimitar";
    private final static String PASSWORD = "dimitar";
    
    private final AttachmentDAO attachmentDAO = new AttachmentDAOImpl();;
    
    /**
     * Seed the database with sample data using SQL scripts.
     * @throws SQLException
     * @throws IOException 
     */
    public void seed() throws SQLException, IOException{
        final String dropAllTablesScript = loadAsString("dropAllTables.sql");
        final String seedAllTablesScript = loadAsString("createAndSeedTables.sql");

        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            for (String statement : splitStatements(new StringReader(dropAllTablesScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
            for (String statement : splitStatements(new StringReader(seedAllTablesScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
            
            seedAttachment();
        }
    }
    
    /**
     * Utility method for loading a script as a String.
     * @param path
     * @return 
     */
    private String loadAsString(final String path){
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }
    
    /**
     * Utility method that splits SQL statements in a script
     * @param reader
     * @param statementDelimiter
     * @return the list of statements
     */
    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }
    
    /**
     * Utility method for checking if a String line is an SQL comment
     * @param line
     * @return true if it a comment, false if it is not
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
    
    /**
     * Utility method that seeds the attachment table with sample attachments.
     * @throws SQLException
     * @throws IOException 
     */
    private void seedAttachment() throws SQLException, IOException{
        BufferedImage image = ImageIO.read(new File("FreeFall.jpg"));
        BufferedImage image2 = ImageIO.read(new File("WindsorKen180.jpg"));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", bos);
        ImageIO.write(image2, "jpg", bos2);
        byte[] imgData = bos.toByteArray();
        byte[] imgData2 = bos2.toByteArray();
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);){
            attachmentDAO.createAttachment(1, "WindsorKen180.jpg", imgData2, "regular", connection);
            attachmentDAO.createAttachment(3, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(6, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(10, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(12, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(12, "WindsorKen180.jpg", imgData2, "embedded", connection);
            attachmentDAO.createAttachment(13, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(17, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(22, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(25, "FreeFall.jpg", imgData, "embedded", connection);
            attachmentDAO.createAttachment(3, "WindsorKen180.jpg", imgData2, "regular", connection);
            attachmentDAO.createAttachment(3, "FreeFall.jpg", imgData, "regular", connection);
            attachmentDAO.createAttachment(4, "WindsorKen180.jpg", imgData2, "regular", connection);
        }
    }
}
