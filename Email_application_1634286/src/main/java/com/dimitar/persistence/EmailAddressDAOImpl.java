package com.dimitar.persistence;

import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the EmailAddressDAO interface methods for CRUD operations on the
 * Email_address table in the Email database
 * 
 * @author Dimitar
 */
public class EmailAddressDAOImpl implements EmailAddressDAO {
    private final AddressDAO addressDAO = new AddressDAOImpl();
    private final static Logger LOG = LoggerFactory.getLogger(EmailAddressDAOImpl.class);
    
    //CREATE
    /**
     * Creates a email_address record from supplied information inside the database
     * @param emailID
     * @param emailAddr
     * @param type
     * @param connection
     * @return the number of inserted email_address records (should always be 1)
     * @throws SQLException 
     */
    @Override
    public int createEmailAddressEntry(int emailID, String emailAddr, String type, Connection connection) throws SQLException {
        int result;
        String createEmailAddressQuery = "INSERT INTO email_address (email_id, address_id, type) VALUES(?, ?, ?)";
        try(PreparedStatement createEmailAddressStmnt = connection.prepareStatement(createEmailAddressQuery, Statement.RETURN_GENERATED_KEYS);){
            createEmailAddressStmnt.setInt(1, emailID);
            createEmailAddressStmnt.setInt(2, addressDAO.findIDOfAddress(emailAddr, connection));
            createEmailAddressStmnt.setString(3, type);
            result = createEmailAddressStmnt.executeUpdate();
        }
        LOG.info(result+" email_address records have been inserted");
        return result;
    }
    
}
