package com.dimitar.beans;

import java.io.Serializable;
import java.time.LocalDateTime;
import jodd.mail.EmailAddress;
import javafx.beans.property.*;

/**
 * Bean for Jodd mail
 * @author Dimitar
 * @version 1.0
 */
public class EmailData implements Serializable{
    
    private static final long serialVersionUID = 7526472295622776147L;
    
    //Basic email properties
    private IntegerProperty id;
    private StringProperty from;
    private ObjectProperty<EmailAddress[]> to;
    private ObjectProperty<EmailAddress[]> CC;
    private ObjectProperty<EmailAddress[]> BCC;
    private StringProperty subject;
    private StringProperty textMessage;
    private StringProperty htmlMessage;
    private ObjectProperty<AttachmentData[]> attachments;
    private ObjectProperty<AttachmentData[]> embeddedAttachments;
    private ObjectProperty<LocalDateTime> sendTime;
    private ObjectProperty<LocalDateTime> receiveTime;
    private StringProperty folderName;
    private IntegerProperty messagePriority;

    /**
     * Constructor to create an email bean
     * @param id
     * @param from
     * @param to
     * @param CC
     * @param BCC
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param embeddedAttachments
     * @param sendTime
     * @param receiveTime
     * @param folderName
     * @param messagePriority 
     */
    public EmailData(int id, String from, EmailAddress[] to, EmailAddress[] CC, EmailAddress[] BCC, String subject, String textMessage, String htmlMessage, AttachmentData[] attachments, AttachmentData[] embeddedAttachments, LocalDateTime sendTime, LocalDateTime receiveTime, String folderName, int messagePriority) {
        this.id = new SimpleIntegerProperty(id);
        this.from = new SimpleStringProperty(from);
        this.to = new SimpleObjectProperty<>(to);
        this.CC = new SimpleObjectProperty<>(CC);
        this.BCC = new SimpleObjectProperty<>(BCC);
        this.subject = new SimpleStringProperty(subject);
        this.textMessage = new SimpleStringProperty(textMessage);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);
        this.attachments = new SimpleObjectProperty<>(attachments);
        this.embeddedAttachments = new SimpleObjectProperty<>(embeddedAttachments);
        this.sendTime = new SimpleObjectProperty<>(sendTime);
        this.receiveTime = new SimpleObjectProperty<>(receiveTime);
        this.folderName = new SimpleStringProperty(folderName);
        this.messagePriority = new SimpleIntegerProperty(messagePriority);
    }
    
    /**
     * Chained constructor that will be used by the receiveEmail method to reconstruct the email bean 
     * @param from
     * @param to
     * @param CC
     * @param subject
     * @param textMessage
     * @param htmlMessage
     * @param attachments
     * @param embeddedAttachments
     * @param sendTime
     * @param receiveTime
     * @param messagePriority 
     */
    public EmailData(String from, EmailAddress[] to, EmailAddress[] CC, String subject, String textMessage, String htmlMessage, AttachmentData[] attachments, AttachmentData[] embeddedAttachments, LocalDateTime sendTime, LocalDateTime receiveTime, int messagePriority){
        this(0,from, to, CC, null, subject, textMessage, htmlMessage, attachments, embeddedAttachments, sendTime, receiveTime, null, messagePriority);
    }
    
    public EmailData(){
        this(0, null, null, null, null, null, null, null, null, null, null, null, null, 0);
    }
    
    public EmailData(String from, EmailAddress[] to, EmailAddress[] CC, EmailAddress[] BCC, String subject, String textMessage, String htmlMessage, AttachmentData[] attachments, AttachmentData[] embeddedAttachments, LocalDateTime sendTime, LocalDateTime receiveTime, String folderName, int messagePriority){
        this(0, from, to, CC, BCC, subject, textMessage, htmlMessage, attachments, embeddedAttachments, sendTime, receiveTime, folderName, messagePriority);
    }
    
    /**
     * Custom implementation of the compareTo method to compare 2 email beans
     * @param email
     * @return 0 if equal -1 if not
     */ 
    public int compareTo(EmailData email){
        //Compare ID
        if(this.getID() != email.getID()){
            return -1;
        }
        //Compare subjects (IF THERE IS ANY)
        if(this.getSubject() != null){
            if(this.getSubject().compareTo(email.getSubject()) != 0){
                return -1;
            }
        }
        //Compare sender
        if(this.getFrom().compareTo(email.getFrom()) != 0){
            return -1;
        }
        //Compare recipients
        if(this.getTo().length != email.getTo().length){
            return -1;
        }
        for(int i=0; i<this.getTo().length; i++){
            //Comparing email addresses
            if(this.getTo()[i].getEmail().compareTo(email.getTo()[i].getEmail()) != 0){
                return -1;
            }
        }
        //Compare CC if there are any
        if (this.getCC() != null && email.getCC() != null) {
            if (this.getCC().length != email.getCC().length) {
                return -1;
            }
            for (int i = 0; i < this.getCC().length; i++) {
                //Comparing email addresses
                if (this.getCC()[i].getEmail().compareTo(email.getCC()[i].getEmail()) != 0) {
                    return -1;
                }
            }
        }
        //Compare text message
        if(this.getTextMessage().compareTo(email.getTextMessage()) != 0){
            return -1;
        }
        //Compare html message
        if(this.getHtmlMessage().compareTo(email.getHtmlMessage()) != 0){
            return -1;
        }
        //Compare embedded attachments (IF THERE ARE ANY)
        if(this.getEmbeddedAttachments() != null){
            if(this.getEmbeddedAttachments().length != email.getEmbeddedAttachments().length){
                return -1;
            }
            for(int i=0; i<this.getEmbeddedAttachments().length; i++){
                if(this.getEmbeddedAttachments()[i].compareTo(email.getEmbeddedAttachments()[i]) != 0){
                    return -1;
                }
            }
        }
        //Compare attachments (IF THERE ARE ANY)
        if(this.getAttachments() != null){
            if(this.getAttachments().length != email.getAttachments().length){
                return -1;
            }
            for(int i=0; i<this.getAttachments().length; i++){
                if(this.getAttachments()[i].compareTo(email.getAttachments()[i]) != 0){
                    return -1;
                }
            }
        }

        return 0;
    }

    //Getters and setters
    public int getID(){
        return this.id.get();
    }
    
    public void setID(int id){
        this.id.set(id);
    }
    
    public IntegerProperty idProperty(){
        return this.id;
    }
    
    public String getFrom() {
        return from.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }
    
    public StringProperty fromProperty(){
        return this.from;
    }

    public EmailAddress[] getTo() {
        return to.get();
    }

    public void setTo(EmailAddress[] to) {
        this.to.set(to);
    }
    
    public ObjectProperty<EmailAddress[]> toProperty(){
        return this.to;
    }

    public EmailAddress[] getCC() {
        return CC.get();
    }

    public void setCC(EmailAddress[] CC) {
        this.CC.set(CC);
    }
    
    public ObjectProperty<EmailAddress[]> ccProperty(){
        return this.CC;
    }

    public EmailAddress[] getBCC() {
        return BCC.get();
    }

    public void setBCC(EmailAddress[] BCC) {
        this.BCC.set(BCC);
    }
    
    public ObjectProperty<EmailAddress[]> bccProperty(){
        return this.BCC;
    }

    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }
    
    public StringProperty subjectProperty(){
        return this.subject;
    }

    public String getTextMessage() {
        return textMessage.get();
    }

    public void setTextMessage(String testMessage) {
        this.textMessage.set(testMessage);
    }
    
    public StringProperty textMessageProperty(){
        return this.textMessage;
    }

    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }
    
    public StringProperty htmlMessageProperty(){
        return this.htmlMessage;
    }

    public AttachmentData[] getAttachments() {
        return attachments.get();
    }

    public void setAttachments(AttachmentData[] attachments) {
        this.attachments.set(attachments);
    }
    
    public ObjectProperty<AttachmentData[]> attachmentsProperty(){
        return this.attachments;
    }

    public AttachmentData[] getEmbeddedAttachments() {
        return embeddedAttachments.get();
    }

    public void setEmbeddedAttachments(AttachmentData[] embeddedAttachments) {
        this.embeddedAttachments.set(embeddedAttachments);
    }
    
    public ObjectProperty<AttachmentData[]> embeddedAttachmentsProperty(){
        return this.embeddedAttachments;
    }

    public LocalDateTime getSendTime() {
        return sendTime.get();
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime.set(sendTime);
    }
    
    public ObjectProperty<LocalDateTime> sendTimeProperty(){
        return this.sendTime;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime.get();
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime.set(receiveTime);
    }
    
    public ObjectProperty<LocalDateTime> receiveTimeProperty(){
        return this.receiveTime;
    }

    public String getFolderName() {
        return folderName.get();
    }

    public void setFolderName(String folderName) {
        this.folderName.set(folderName);
    }
    
    public StringProperty folderNameProperty(){
        return this.folderName;
    }

    public int getMessagePriority() {
        return messagePriority.get();
    }

    public void setMessagePriority(int messagePriority) {
        this.messagePriority.set(messagePriority);
    }
    
    public IntegerProperty messagePriorityProperty(){
        return this.messagePriority;
    }
    
    
}
