package com.dimitar.beans;

import java.io.Serializable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The bean class that represents an email file attachment
 * @author Dimitar
 */
public class AttachmentData implements Serializable{
    
    private static final long serialVersionUID = 7588472295622776147L;
    
    //Basic attachment properties
    private StringProperty name;
    private StringProperty filePath;
    private ObjectProperty<byte[]> attachment;
    
    public AttachmentData(String name, String filePath, byte[] attachment){
        this.name = new SimpleStringProperty(name);
        this.filePath = new SimpleStringProperty(filePath);
        this.attachment = new SimpleObjectProperty<>(attachment);
    }
    
    /**
     * Chained constructor used to create an attachment without a file path
     * @param name
     * @param attachment 
     */
    public AttachmentData(String name, byte[] attachment){
        this(name, null, attachment);
    }
    
    /**
     * Default constructor
     */
    public AttachmentData(){
        this(null, null, null);
    }

    /**
     * Custom implementation of compareTo method to compare 2 attachment beans
     * @param attachment
     * @return 0 if equal -1 if not
     */
    public int compareTo(AttachmentData attachment){
        //Compare attachment file name
        if(this.getName().compareTo(attachment.getName()) != 0){
            return -1;
        }
        return 0;
    }
    //Getters and setters
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }
    
    public StringProperty nameProperty(){
        return this.name;
    }
    
    public String getFilePath(){
        return filePath.get();
    }
    
    public void setFilePath(String filePath){
        this.filePath.set(filePath);
    }
    
    public StringProperty filePathProperty(){
        return this.filePath;
    }

    public byte[] getAttachment() {
        return attachment.get();
    }

    public void setAttachment(byte[] attachment) {
        this.attachment.set(attachment);
    }
    
    public ObjectProperty<byte[]> attachmentProperty(){
        return this.attachment;
    }
    
    
}
