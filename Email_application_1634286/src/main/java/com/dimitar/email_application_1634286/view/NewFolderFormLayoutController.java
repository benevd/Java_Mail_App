package com.dimitar.email_application_1634286.view;

import com.dimitar.persistence.EmailDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller class for the New Folder layout view.
 *
 * @author Dimitar
 */
public class NewFolderFormLayoutController {
    
    private final static Logger LOG = LoggerFactory.getLogger(NewFolderFormLayoutController.class);
    //FXML Fields
    @FXML
    private AnchorPane newFolderView;
    
    @FXML
    private TextField newFolderNameTxt;
    
    @FXML
    private Button createFolderBtn;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //DAO reference
    private EmailDAO emailDAO;
    
    //Root layout controller reference for closing the form
    private RootLayoutController rootController;
    //Folder tree layout controller reference for refreshing the list of folders
    private FolderTreeLayoutController folderTreeController;
    //Email table view layout controller reference to refresh the list of folders
    private EmailTableLayoutController emailTableController;

    /**
     * Initializes the controller class.
     */
    public void initialize() {
        //Assign a button handler for creating a folder
        createFolderBtn.setOnMouseClicked((event) -> {
            try {
                this.createNewFolder(event);
            } catch (IOException e) {
                LOG.error("Connection to the database has caused an error", e);
                Platform.exit();
            }
        });
    }  
    
    /**
     * Set the reference to emailDAO in the new folder layout view window
     * @param emailDAO 
     */
    public void setEmailDAO(EmailDAO emailDAO){
        this.emailDAO = emailDAO;
    }
    
    /**
     * Set the reference to the root layout controller so that the form creation
     * window may be closed from this class.
     * @param rootController 
     */
    public void setRootController(RootLayoutController rootController){
        this.rootController = rootController;
    }
    
    /**
     * Set the reference to the folder tree layout controller so it can be
     * updated with new folders.
     * @param controller 
     */
    public void setFolderTreeController(FolderTreeLayoutController controller){
        this.folderTreeController = controller;
    }
    
    /**
     * Set the reference to the email table layout controller so the folders
     * can be refreshed in the context menus.
     * @param controller 
     */
    public void setTableViewController(EmailTableLayoutController controller){
        this.emailTableController = controller;
    }
    
    /**
     * Button event handler that creates a new folder.
     * @param event 
     */
    private void createNewFolder(MouseEvent event) throws IOException{
        //Get the name of the new folder
        String name = newFolderNameTxt.getText();
        Alert dialog;
        //Pop an error dialog
        if(name.isEmpty()){
            dialog = new Alert(AlertType.ERROR, resources.getString("EmptyFolder"));
            dialog.setHeaderText(null);
            dialog.setTitle(null);
            dialog.show();
        }
        else{
            try{
                emailDAO.createFolder(name);
                //Refresh the folder tree view
                folderTreeController.addNewFolderToTree();
                //Refresh the folder list in the email table view controller
                emailTableController.setFolders();
                //Close the stage (window) when a folder was successfully created
                rootController.closeFolderFormWindow();
            }
            catch(SQLException se){
                dialog = new Alert(AlertType.ERROR, resources.getString("FolderExists"));
                dialog.setHeaderText(null);
                dialog.setTitle(null);
                dialog.show();
            }
        }  
    }
    
}
