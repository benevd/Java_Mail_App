package com.dimitar.email_application_1634286.view;

import com.dimitar.business.MainAppFX;
import com.dimitar.business.SendEmail;
import java.io.IOException;
import java.io.OutputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * FXML Controller class
 *
 * @author Dimitar
 */
public class RegisterEmailLayoutController {
    private final static Logger LOG = LoggerFactory.getLogger(RegisterEmailLayoutController.class);
    //FXML fields
    @FXML
    private ResourceBundle resources;
    @FXML
    private TextField emailAddressTxt;
    @FXML
    private PasswordField emailPasswordTxt;
    @FXML
    private Button registerBtn;
    @FXML
    private TextField imapURL;
    @FXML
    private TextField smtpURL;
    @FXML
    private TextField dbURL;
    @FXML
    private TextField dbUser;
    @FXML
    private PasswordField dbPassword;
    @FXML
    private Button saveConfigBtn;
    
    //Reference to MainAppFX so it can tell it to launch the main application once it is done registering
    private MainAppFX mainApp;
    
    //Reference to SendEmail so it can check for email validity and check if the user successfully can sign in
    private SendEmail sendEmail;
    
    //Reference to the properties objects so they can be set
    private Properties credentials;
    private Properties db;
    
    //Reference to the Path of the properties files
    private Path credentialsPath;
    private Path dbPath;
    
    //Reference to the RootLayoutController to be able to refresh the credentials
    private RootLayoutController rootController;
    
    //Status variable that keeps track of if the user has requested a configuration change
    private boolean hasRequestForModifyConfig = false;
    
    //Other properties values for checking if they were set before starting the app
    private String imapServer;
    private String smtpServer;
    
    /**
     * Initializes the controller class.
     */
    public void initialize() {
        //Initialize the sendEmail class
        sendEmail = new SendEmail();
        
        //Set the button handler for register button
        registerBtn.setOnMouseClicked(this::registerEmailAccount);
        
        //Set the button handler for setting db config
        saveConfigBtn.setOnMouseClicked(this::saveDBConfig);
    }
    
    /**
     * Set the reference to the main application for display of the email app
     * once email has been registered.
     * @param app 
     */
    public void setMainAppFX(MainAppFX app){
        this.mainApp = app;
    }
    
    /**
     * Set the reference to the properties object so it can set the missing values
     * to connect a user.
     * @param props 
     */
    public void setCredentialsProperties(Properties props){
        this.credentials = props;
    }
    
    /**
     * Set the reference to the Database properties object so the properties can
     * be set/manipulated from this controller.
     * @param props 
     */
    public void setDBProperties(Properties props){
        this.db = props;
    }
    
    /**
     * Set the reference to the properties object Path so it can be re-used to
     * write save the newly written properties.
     * @param path 
     */
    public void setPropertiesPath(Path path){
        this.credentialsPath = path;
    }
    
    /**
     * Set the reference to the db properties object Path so the controller can
     * write the saved properties to the file.
     * @param path 
     */
    public void setDBPropertiesPath(Path path){
        this.dbPath = path;
    }
    
    /**
     * Set the status of the configuration modifying request. It lets the controller
     * know that the configurations are being modified in-app and not when opening
     * the app for the first time.
     */
    public void setConfigurationStatusRequest(){
        this.hasRequestForModifyConfig = true;
    }
    
    /**
     * Set the root controller reference so this controller can send information 
     * regarding the user's credentials to the root.
     * @param controller 
     */
    public void setRootControllerReference(RootLayoutController controller){
        this.rootController = controller;
    }

    /**
     * Register an email account with the email application. It checks for validity
     * of email address and password and sets up the folders.
     */
    private void registerEmailAccount(MouseEvent event) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setTitle(null);
        //Check if the db properties have been set
        //CHECK IF ALL THE OTHER PROPERTIES ARE ALREADY SET
        if (!checkForDBPropertiesSet()) {
            alert.setContentText(resources.getString("SaveDBConfigAlert"));
            alert.show();
        } 
        else {
            //Get the values of the text and password fields
            String user = emailAddressTxt.getText();
            String password = emailPasswordTxt.getText();
            String smtpServ;
            if(!smtpURL.getText().isEmpty()){
                smtpServ = smtpURL.getText();
            }
            //If the smtp server field was emptied, set to default
            else{
                smtpServ = "smtp.gmail.com";
            }
            //Verify the email address
            if (sendEmail.checkEmail(user)) {
                //If valid check if the user can sign in
                if (sendEmail.tryConnecting(user, password, smtpServ)) {
                    LOG.info("Can sign in");
                    //Set the properties file
                    credentials.setProperty("username", user);
                    credentials.setProperty("password", password);
                    //Save the properties
                    try (OutputStream propFileStream = newOutputStream(credentialsPath, StandardOpenOption.CREATE);) {
                        credentials.store(propFileStream, "Email credential properties");
                    } catch (IOException e) {
                        LOG.error("Loading the properties file has caused an error", e);
                        Platform.exit();
                    }
                    //If setting properties at app launch
                    if (hasRequestForModifyConfig == false) {
                        //Send the credentials to the application and open it
                        mainApp.showEmailAppAfterRegistration(user, password, imapServer, smtpServer, true);

                    } //If modifying properties in-app
                    else {
                        //Set the credentials in the root layout
                        rootController.setCredentials(user, password, imapServer, smtpServer);
                        //Popup an alert notifying the user that the configuration has been successfully modified
                        Alert info = new Alert(Alert.AlertType.INFORMATION);
                        info.setHeaderText(null);
                        info.setTitle(null);
                        info.setContentText(resources.getString("ConfigChanged") + user);
                        info.show();
                        //Clear the lower right pane to refresh credentials
                        rootController.clearLowerRightPaneIfOpened();
                        //Close this window
                        mainApp.hideConfigWindow();
                        hasRequestForModifyConfig = false;
                    }
                } else {
                    //Popup a message to let the user know that either the address or password is invalid
                    alert.setContentText(resources.getString("ErrorSigningIn"));
                    alert.show();
                    //Clear the fields
                    emailAddressTxt.clear();
                    emailPasswordTxt.clear();
                }
            } else {
                //Popup a message to let the user know their email address is invalid
                alert.setContentText(resources.getString("AddressDoesNotExist"));
                alert.show();
                //Clear the field
                emailAddressTxt.clear();
            }
        }
    }
    
    /**
     * Saves all the configuration changes done under the heading 'DB configuration'
     * @param event 
     */
    private void saveDBConfig(MouseEvent event){
        //Setting the modifications to properties
        if(!imapURL.getText().isEmpty()){
            imapServer = imapURL.getText();
            credentials.setProperty("imapURL", imapServer);
            rootController.setIMAPServer(imapServer);
        }
        if(!smtpURL.getText().isEmpty()){
            smtpServer = smtpURL.getText();
            credentials.setProperty("smtpURL", smtpServer);
            rootController.setSMTPServer(smtpServer);
        }
        if(!dbURL.getText().isEmpty()){
            db.setProperty("dbURL", dbURL.getText());
        }
        if(!dbUser.getText().isEmpty()){
            db.setProperty("dbUser", dbUser.getText());
        }
        if(!dbPassword.getText().isEmpty()){
            db.setProperty("dbPassword", dbPassword.getText());
        }
        //Save to the file
        //Save the properties in credential
        try (OutputStream propFileStream = newOutputStream(credentialsPath, StandardOpenOption.CREATE);) {
            credentials.store(propFileStream, "Email credential properties");
        } catch (IOException e) {
            LOG.error("Loading the properties file has caused an error", e);
            Platform.exit();
        }
        //Save the properties in db
        try (OutputStream propFileStream = newOutputStream(dbPath, StandardOpenOption.CREATE);) {
            db.store(propFileStream, "DB application properties");
        } catch (IOException e) {
            LOG.error("Loading the properties file has caused an error", e);
            Platform.exit();
        }
        //Popup a dialog notifying the user that the db properties have been set
        Alert info = new Alert(Alert.AlertType.INFORMATION);
        info.setHeaderText(null);
        info.setTitle(null);
        info.setContentText(resources.getString("ConfigSaved"));
        info.show();
        if(hasRequestForModifyConfig){
            mainApp.hideConfigWindow();
        } 
    }
    
    /**
     * Utility method that checks if the Database properties have been set.
     * @return 
     */
    private boolean checkForDBPropertiesSet(){
        return db.containsKey("dbURL") && db.containsKey("dbUser") && db.containsKey("dbPassword");
    }
    
}
