package com.dimitar.email_application_1634286.view;

import com.dimitar.business.MainAppFX;
import com.dimitar.persistence.EmailDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for the folder tree layout. It is contained inside the left split
 * of the root layout. It displays the list of folders, and when a folder is 
 * selected the upper right split displays the table with emails contained
 * within that folder.
 *
 * @author Dimitar
 */
public class FolderTreeLayoutController {
    private final static Logger LOG = LoggerFactory.getLogger(FolderTreeLayoutController.class);
    
    //Handle to the Email DAO 
    private EmailDAO emailDAO;
    
    //Handle to the email table layout controller
    private EmailTableLayoutController emailTableController;
    //Handle to the folder rename layout controller
    private FolderRenameFormLayoutController folderRenameController;
    
    //FXML fields
    @FXML
    private TreeView<String> folderTreeView;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Folder rename stage reference
    private Stage folderStage;
    
    /**
     * Initializes the tree folder layout.
     */
    @FXML
    public void initialize() {
        //Root node for the tree
        String folderRoot = resources.getString("Folders");
        
        folderTreeView.setRoot(new TreeItem<>(folderRoot));
        
        //Cell factory used to set the content of each tree node
        folderTreeView.setCellFactory((e) -> new TreeCell<String>(){
            @Override
            protected void updateItem(String item, boolean empty){
                super.updateItem(item, empty);
                if(item != null){
                    if (!item.equals("received") && !item.equals("sent")) {
                        //Attach a context menu to the current folder
                        ContextMenu menu = new ContextMenu();
                        MenuItem rename = new MenuItem(resources.getString("Rename"));
                        rename.setOnAction((event) -> {
                            renameFolderAction(item);
                        });
                        MenuItem delete = new MenuItem(resources.getString("Delete"));
                        delete.setOnAction( (event) -> {
                            deleteFolderAction(item);
                        });
                        menu.getItems().addAll(rename, delete);
                        setContextMenu(menu);
                    }
                    setText(item);
                    setGraphic(getTreeItem().getGraphic());
                }
                else{
                    setText("");
                    setGraphic(null);
                }
            }
        });
    }
    
    /**
     * Event handler for context menu item 'Rename' which pops up a windows with
     * a prompt to enter a new name for the folder on which is was called on.
     * @param folder 
     */
    private void renameFolderAction(String folder){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/FolderRenameFormLayout.fxml"));
            AnchorPane folderRenameView = (AnchorPane) loader.load();
            //Set the reference to the controller
            folderRenameController = loader.getController();
            //Send a reference to the DAO to the folder rename controller
            folderRenameController.setEmailDAO(emailDAO);
            //Send a reference to this controller to the folder rename controller so it can refresh the tree view
            folderRenameController.setFolderTreeController(this);
            //Send the folder on which the context menu was invoked on
            folderRenameController.setFolderToRename(folder);
            //Send a reference to the email table controller so it can update the folder list
            folderRenameController.setEmailTableController(emailTableController);
            
            //Show the window
            folderStage = new Stage();
            folderStage.setScene(new Scene(folderRenameView));
            folderStage.initStyle(StageStyle.UTILITY);
            folderStage.show();
        }
        catch(IOException e){
            LOG.error("Displaying the renaming folder form has caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Event handler for context menu item "Delete". It deletes the folder on
     * which the context menu item was called on.
     * @param folder 
     */
    private void deleteFolderAction(String folder){
        try{
            //Pop a confirmation dialog so the user is certain that they want to delete the folder
            Alert confirmation = new Alert(AlertType.CONFIRMATION);
            confirmation.setTitle(null);
            confirmation.setHeaderText(null);
            confirmation.setContentText(resources.getString("DeleteFolder")+" '"+folder+"'?");
            Optional<ButtonType> choice = confirmation.showAndWait();
            if (choice.get() == ButtonType.OK) {
                emailDAO.deleteFolder(folder);
                //Remove the deleted folder from the tree view
                int itemToRemove = folderTreeView.getSelectionModel().getSelectedIndex() - 1;
                folderTreeView.getRoot().getChildren().remove(itemToRemove);
                //Update the folder list in the table
                emailTableController.setFolders();
            }
        }
        catch(SQLException e){
            LOG.error("Deleting the folder has caused an error", e);
            Platform.exit();
        }
        catch(IOException e){
            LOG.error("There was an issue with connecting to the database", e);
            Platform.exit();
        }
            
    }

    /**
     * Called inside RootLayoutController to provide a reference to the EmaiLDAO
     * object.
     * @param emailDAO 
     */
    public void setEmailDAO(EmailDAO emailDAO){
        this.emailDAO = emailDAO;
    }
    
    /**
     * Called inside RootLayoutController to provide a reference to the table
     * layout controller.
     * @param controller 
     */
    public void setTableController(EmailTableLayoutController controller){
        this.emailTableController = controller;
    }
    
    /**
     * Displays the folder tree from information from the database.
     * 
     * @throws SQLException 
     */
    public void displayFolderTree() throws SQLException, IOException{
        //Retrieve the list of folder name Strings 
        List<String> folders = emailDAO.findAllFolders();
        
        //Build an item for each folder and add it to the root
        if(folders != null){
            folders.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/folder_icon.png").toExternalForm()));
                return item;
            }).forEachOrdered((item) -> {
                folderTreeView.getRoot().getChildren().add(item);
            });
        }
        
        //Expand the tree view
        folderTreeView.getRoot().setExpanded(true);
        
        //Listen for selection changes to open the right folder in the email data table
        folderTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                showFolder(newValue);
            } catch (SQLException e) {
               LOG.error("Error related to SQL", e);
               Platform.exit();
            } catch (IOException e) {
               LOG.error("Error during SQL connection setting", e);
               Platform.exit();
            }
        });
        
        //Set the default selected folder to received (inbox)
        TreeItem<String> received = folderTreeView.getTreeItem(2);
        folderTreeView.getSelectionModel().select(received);
    }
    
    /**
     * Refreshes the folder tree view by adding the folder that has just been
     * created in the new folder form. Used by the NewFolderFormLayoutController
     * class.
     * @throws SQLException
     * @throws IOException 
     */
    public void addNewFolderToTree() throws SQLException, IOException{
        //Retrieve the List of folders and get the last inserted folder
        List<String> folders = emailDAO.findAllFolders();
        String newFolder = folders.get(folders.size() - 1);
        
        //Build a new tree view item and add it to the root of the tree
        TreeItem<String> item = new TreeItem<>(newFolder);
        item.setGraphic(new ImageView(getClass().getResource("/images/folder_icon.png").toExternalForm()));
        
        folderTreeView.getRoot().getChildren().add(item);
    }
    
    /**
     * Displays all the emails in the current folder inside the table view.
     * @param folder
     * @throws SQLException
     * @throws IOException 
     */
    private void showFolder(TreeItem<String> folder) throws SQLException, IOException{
        //Change the table to display emails that belong to this folder
        emailTableController.displayTable(folder.getValue());
    }
    
    /**
     * Refreshes the folder who has just been renamed to reflect the changes. It
     * is done by removing it from the tree view and re-adding it.
     * @param oldName
     * @param newName
     */
    public void refreshFolderWithNewName(String oldName, String newName){
        //Build a tree item that will replace the one with the old name
        TreeItem<String> item = new TreeItem<>(newName);
        item.setGraphic(new ImageView(getClass().getResource("/images/folder_icon.png").toExternalForm()));
        //Get the folder that will be removed
        int itemToRemove = folderTreeView.getSelectionModel().getSelectedIndex() -1;
        //Remove the folder with the old name
        folderTreeView.getRoot().getChildren().remove(itemToRemove);
        //Add the folder with the new name
        folderTreeView.getRoot().getChildren().add(itemToRemove, item);
        //Set the selection back to the folder
        folderTreeView.getSelectionModel().select(itemToRemove+1);
    }
    /**
     * Called inside the folder rename controller to close the stage once renaming
     * a folder is completed.
     */
    public void closeRenameFolderStage(){
        folderStage.close();
    }
}
