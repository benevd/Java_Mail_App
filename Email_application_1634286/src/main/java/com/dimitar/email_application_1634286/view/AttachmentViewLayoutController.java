package com.dimitar.email_application_1634286.view;

import com.dimitar.beans.AttachmentData;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javax.imageio.ImageIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Dimitar
 */
public class AttachmentViewLayoutController{
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentViewLayoutController.class);
    //FXML fields
    @FXML
    private Text attachmentNameTxt;
    @FXML
    private ImageView imageView;
    @FXML
    private Button downloadBtn;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Currently opened attachment
    private AttachmentData currentAttachment;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        downloadBtn.setOnMouseClicked(this::downloadAttachment);
    }
    
    /**
     * Display a regular email attachment in a separate window.
     * @param attachment 
     */
    public void displayAttachment(AttachmentData attachment){
        //Check if an attachment handle was successfully passed
        if(attachment != null){
            //Set the attachment value to be used by download
            currentAttachment = attachment;
            //Display the file name
            attachmentNameTxt.setText(attachment.getName());
            //Display the image
            ByteArrayInputStream imgStream=new ByteArrayInputStream(attachment.getAttachment());
            Image img = new Image(imgStream);
            imageView.setImage(img);
        }
        else{
            LOG.error("ERROR - No attachment was passed");
            Platform.exit();
        }
    }
    
    /**
     * Allows for an attachment to be downloaded. The user can select in which
     * directory they want the file to be saved to.
     * @param event 
     */
    public void downloadAttachment(MouseEvent event){
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle(resources.getString("ChooseDownloadLocation"));
        File defaultDirectory = new File("C:/Temp");
        chooser.setInitialDirectory(defaultDirectory);
        String selectedDirectory ="";
        try{
            selectedDirectory = chooser.showDialog(null).toString();
        }
        catch(NullPointerException e){
            LOG.info("Chooser closed unexpectedly");
        }
        try {
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(currentAttachment.getAttachment()));
            //Get the type of the current image
            String fileName = currentAttachment.getName();
            String type = fileName.substring(fileName.indexOf(".") + 1, fileName.length());
            File newFile = new File(selectedDirectory, fileName);
            ImageIO.write(bufferedImage, type, newFile);
        } 
        catch (IOException ex) {
            LOG.error("Saving the file has caused an error", ex);
            Platform.exit();
        }
    }
    
}
