package com.dimitar.email_application_1634286.view;

import com.dimitar.persistence.EmailDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Dimitar
 */
public class FolderRenameFormLayoutController {
    private final static Logger LOG = LoggerFactory.getLogger(FolderRenameFormLayoutController.class);
    //FXML fields
    @FXML
    private TextField newFolderName;
    @FXML
    private Button renameBtn;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Handle to EmailDAO
    private EmailDAO emailDAO;
    
    //Handle to the FolderTreeLayoutController & EmailTableLayoutController for updating folder info
    private FolderTreeLayoutController folderTreeController;
    private EmailTableLayoutController emailTableController;
    
    //Current folder that needs to be renamed
    private String currentFolder;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        //Set the event handler for the "Rename" button
        renameBtn.setOnMouseClicked(this::renameFolder);
    } 
    
    /**
     * Called in FolderTreeLayoutController to set the reference to the email DAO
     * so this controller is able to rename folders in the database.
     * @param emailDAO 
     */
    public void setEmailDAO(EmailDAO emailDAO){
        this.emailDAO = emailDAO;
    }
    
    /**
     * Sets the currently selected folder value that will be renamed.
     * @param folder 
     */
    public void setFolderToRename(String folder){
        this.currentFolder = folder;
    }
    
    /**
     * Set a reference to the folder tree controller here so it can be updated with
     * the new folder name.
     * @param controller 
     */
    public void setFolderTreeController(FolderTreeLayoutController controller){
        this.folderTreeController = controller;
    }
    
    /**
     * Set a reference to the email table controller so its list of folders can
     * be refreshed with new data after renaming.
     * @param controller 
     */
    public void setEmailTableController(EmailTableLayoutController controller){
        this.emailTableController = controller;
    }
    
    /**
     * Renames the folder passed as argument. It closes the window if the renaming
     * was successful otherwise displays appropriate error messages. Once a folder
     * is renamed it refreshes the folder tree to reflect the change.
     * @param event
     */
    public void renameFolder(MouseEvent event){
      //Get the new folder name from the text field
      String newName = newFolderName.getText();
      Alert dialog;
      //Popup an error dialog if the new name is empty
      if(newName.isEmpty()){
          dialog = new Alert(AlertType.ERROR, resources.getString("EmptyFolder"));
          dialog.setHeaderText(null);
          dialog.setTitle(null);
          dialog.show();
      }
      else{
          try{
              emailDAO.renameFolder(currentFolder, newName);
              //Refresh the folder tree view
              folderTreeController.refreshFolderWithNewName(currentFolder, newName);
              //Close the stage (window) once the folder has been renamed successfully
              folderTreeController.closeRenameFolderStage();
              //Update the folder list in the table
              emailTableController.setFolders();
          }
          catch(SQLException | IllegalArgumentException e){
              dialog = new Alert(AlertType.ERROR, resources.getString("FolderExists"));
              dialog.setHeaderText(null);
              dialog.setTitle(null);
              dialog.show();
          } 
          catch (IOException e) {
              LOG.error("Connecting to the database caused an error", e);
          }
      }
    }
    
}
