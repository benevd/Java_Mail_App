package com.dimitar.email_application_1634286.view;

import com.dimitar.beans.EmailData;
import com.dimitar.business.MainAppFX;
import com.dimitar.business.ReceiveEmail;
import com.dimitar.business.SendEmail;
import com.dimitar.persistence.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jodd.mail.MailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller class for the root layout. All the other layouts are added in code
 * here, allowing for the combination of multiple standalone containers inside.
 *
 * @author Dimitar
 */
public class RootLayoutController {
    
    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutController.class);
    
    //FXML fields
    @FXML
    private Button newEmail;
    
    @FXML
    private Button newFolder;
    
    @FXML
    private Button refreshEmails;
    
    @FXML
    private AnchorPane leftSplit;
    
    @FXML
    private AnchorPane upperRightSplit;
    
    @FXML
    private AnchorPane lowerRightSplit;
    
    @FXML
    private MenuItem newEmailItem;

    @FXML
    private MenuItem newFolderItem;

    @FXML
    private MenuItem editConfigItem;

    @FXML
    private MenuItem refreshEmailsItem;
    
    @FXML
    private MenuItem sendEmailItem;
    
    @FXML
    private Menu addAttachment;
    
    @FXML
    private MenuItem addRegularAttachment;
    
    @FXML
    private MenuItem addEmbeddedAttachment;
    
    @FXML
    private Menu removeAttachment;

    @FXML
    private MenuItem aboutItem;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //DAO references
    private final EmailDAO emailDAO;
    
    //Controller handles
    private FolderTreeLayoutController folderTreeController;
    private EmailTableLayoutController emailTableController;
    private EmailWebViewLayoutController emailViewController;
    private EmailHTMLEditorLayoutController emailEditorController;
    private NewFolderFormLayoutController newFolderController;
    private RegisterEmailLayoutController registerController;
    
    //Reference to the MainAppFX class so it can open the config window again
    private MainAppFX mainApp;
    
    //Handle to dynamic views
    private AnchorPane emailView;
    private AnchorPane htmlEditorView;
    
    //Counter for clearing the bottom right split view to refresh it each time it is displaying new content
    private int count;
    
    //Folder form windows stage reference
    Stage folderStage;
    
    //Handles to Send & Receive emails
    private SendEmail sendEmail;
    private ReceiveEmail receiveEmail;
    
    //Credentials
    private String user;
    private String password;
    
    //Email servers info
    private String smtpURL;
    private String imapURL;
    
    /**
     * Default constructor for initializing DAO instances and count.
     */
    public RootLayoutController(){
        super();
        emailDAO = new EmailDAOImpl();
        count = 0;
    }

    /**
     * Call the methods that initialize all the sub containers.
     * @throws java.io.IOException
     */
    @FXML
    public void initialize() throws IOException {
        //Initialize business classes for sending & receiving emails
        sendEmail = new SendEmail();
        receiveEmail = new ReceiveEmail();
        
        //Initialize layouts
        initLeftLayout();
        initTopRightLayout();
        initHTMLEditorLayout();
        initEmailWebView();
        
        //Assign a handler to the new email button/menu item to open the HTML editor layout
        newEmail.setOnMouseClicked(this::displayHTMLEditorLayout);
        newEmailItem.setOnAction( (event) -> {
            displayHTMLEditorLayout(null);
        });
        
        //Assign a handler to the new folder button/menu item to open the folder creation form
        newFolder.setOnMouseClicked(this::displayNewFolderForm);
        newFolderItem.setOnAction( (event) -> {
            displayNewFolderForm(null);
        });
        
        //Assign a handler to the modify config menu item to open the configuration form
        editConfigItem.setOnAction( (event) -> {
            displayRegistrationForm();
        });
        
        //Assign a handler to the about menu item to open the about message window
        aboutItem.setOnAction( (event) -> {
            displayAboutWindow(null);
        });
        
        //Assign a handler to the refresh emails button/ menu item to receive new emails and refresh the GUI
        refreshEmails.setOnMouseClicked((event) -> {
            try {
                this.refreshEmails(event);
            } catch (SQLException e) {
                LOG.error("Refreshing the emails has caused an SQLException", e);
                Platform.exit();
            } catch (IOException e) {
                LOG.error("Error establishing connection to database");
                Platform.exit();
            }
        });
        refreshEmailsItem.setOnAction( (event) -> {
            try {
                this.refreshEmails(null);
            } catch (SQLException e) {
                LOG.error("Refreshing the emails has caused an SQLException", e);
                Platform.exit();
            } catch (IOException e) {
                LOG.error("Error establishing connection to database");
                Platform.exit();
            }
        });
        
        //Send a reference to EmailTableLayoutController to the FolderTreeLayoutController
        setTableControllerToTree();
        
        //Send a reference to EmailWebViewLayoutController to the EmailTableLayoutController
        setEmailViewControllerToTable();
        
        //Send a reference to RootLayoutController to the EmailTableLayoutController so it can display emails dynamically
        setRootControllerToTable();
              
        try{
            folderTreeController.displayFolderTree();
        }
        catch(SQLException e){
            LOG.error("Error during initialization", e);
            Platform.exit();
        }
        
        //Set the handlers for sending email and adding attachments
        sendEmailItem.setOnAction( (event) -> {
            try {
                sendEmail();
            } catch (SQLException ex) {
                LOG.error("Sending email from menubar has caused an error", ex);
                Platform.exit();
            } catch (IOException ex) {
                LOG.error("Sending email from menubar has caused an error", ex);
                Platform.exit();
            }
        });
        
        addRegularAttachment.setOnAction( (event) -> {
            addRegularAttachment();
        });
        
        addEmbeddedAttachment.setOnAction( (event) -> {
            addEmbeddedAttachment();
        });
        
        //Disable the menu items by default until HTML editor is opened
        sendEmailItem.setDisable(true);
        addAttachment.setDisable(true);
        removeAttachment.setDisable(true);
    }
    
    /**
     * Handler for sending email. It uses the method from the html editor controller.
     * @throws SQLException
     * @throws IOException 
     */
    private void sendEmail() throws SQLException, IOException{
        emailEditorController.sendEmail(null);
    }
    
    /**
     * Handler for adding a regular attachment. It uses the method from the html
     * editor controller.
     */
    private void addRegularAttachment(){
        emailEditorController.addAttachment("regular");
    }
    
    /**
     * Handler for adding an embedded attachment. It uses the method from the
     * html editor controller.
     */
    private void addEmbeddedAttachment(){
        emailEditorController.addAttachment("embedded");
    }
    
    /**
     * Utility method used by the editor controller to load the list of removable
     * attachments.
     * @param attach 
     */
    public void loadRemovableAttachment(MenuItem attach){
        removeAttachment.getItems().add(attach);
    }
    
    /**
     * Utility method used by the editor controller to clear the list of removable
     * attachments to add a fresh list when an attachment is added.
     */
    public void clearRemovableAttachments(){
        removeAttachment.getItems().clear();
    }
    
    /**
     * Utility method used by the editor controller to enable/disable the remove
     * attachment menu dynamically depending on if there are attachments.
     * @param enable 
     */
    public void setEnableRemoveAttachments(boolean enable){
        if(enable){
            removeAttachment.setDisable(false);
        }
        else{
            removeAttachment.setDisable(true);
        }
    }
    
    /**
     * Displays the registration form when the user requests a change in the
     * configuration (properties) file.
     */
    private void displayRegistrationForm(){
        registerController.setConfigurationStatusRequest();
        mainApp.showConfigWindow();
    }
    
    /**
     * Sets the reference to the main app.
     * @param mainApp 
     */
    public void setMainAppFXReference(MainAppFX mainApp){
        this.mainApp = mainApp;
    }
    
    /**
     * Set the reference to the table controller inside the tree controller.
     */
    private void setTableControllerToTree(){
        folderTreeController.setTableController(emailTableController);
    }
    
    /**
     * Set the reference to the register controller
     * @param controller 
     */
    public void setRegisterControllerToRoot(RegisterEmailLayoutController controller){
        this.registerController = controller;
    }
    
    /**
     * Set the reference to the email view controller inside the table controller
     */
    private void setEmailViewControllerToTable(){
        emailTableController.setEmailViewController(emailViewController);
    }
    
    /**
     * Set the reference to the root controller inside the table controller for
     * being able to open the email view in the bottom right split via a listener
     * in the table.
     */
    private void setRootControllerToTable(){
        emailTableController.setRootLayoutController(this);
    }
    
    /**
     * Sets the reference to the root controller inside the new folder form layout
     * controller so it can close the stage when a new folder has been successfully
     * created and added.
     */
    private void setRootControllerToNewFolderWindow(){
        newFolderController.setRootController(this);
    }
    
    /**
     * Sets the credential information for sending and receiving emails.
     * @param user
     * @param password 
     * @param imapURL 
     * @param smtpURL 
     */
    public void setCredentials(String user, String password, String imapURL, String smtpURL){
        this.user = user;
        this.password = password;
        this.imapURL = imapURL;
        this.smtpURL = smtpURL;
    }

    /**
     * Initialize the tree view layout (folder list)
     */
    private void initLeftLayout(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/FolderTreeLayout.fxml"));
            AnchorPane treeFolderView = (AnchorPane) loader.load();
            
            //Give the controller the DAO object handle
            folderTreeController = loader.getController();
            folderTreeController.setEmailDAO(emailDAO);
            
            leftSplit.getChildren().add(treeFolderView);
        }
        catch(IOException e){
            LOG.error("initLeftLayout caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Initialize the table view layout (email list)
     */
    private void initTopRightLayout(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/EmailTableLayout.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();
            
            //Give the controller the DAO object
            emailTableController = loader.getController();
            emailTableController.setEmailDAO(emailDAO);
            //Set the list of current folders that emails can be moved to
            emailTableController.setFolders();
            upperRightSplit.getChildren().add(tableView);
        }
        catch(IOException e){
            LOG.error("initTopRightLayout caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Initialize the send email form & editor layout so it is ready to be called
     * dynamically by the button handler for 'New Email'.
     */
    private void initHTMLEditorLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/EmailHTMLEditorLayout.fxml"));
            htmlEditorView = (AnchorPane) loader.load();
            //Give the controller the DAO objects
            emailEditorController = loader.getController();
            emailEditorController.setEmailDAO(emailDAO);
            //Set a reference to this controller
            emailEditorController.setRootController(this);
        } catch (IOException e) {
            LOG.error("Displaying the HTML editor caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Event handler for the 'New Email' button which displays the send email
     * form and the editor in the bottom right split and clears it as needed.
     * @param event 
     */
    private void displayHTMLEditorLayout(MouseEvent event) {
        //Clear the fields if they were previously set
        emailEditorController.clearEmailFields();
        //Set the menu items related to email sending functions to enabled
        sendEmailItem.setDisable(false);
        addAttachment.setDisable(false);
        //Clear lower right pane if needed
        clearLowerRightPaneIfOpened();
        //Send a reference to sendEmail to the email editor controller along with necessary credentials
        sendEmail.setPassword(password);
        sendEmail.setSmtpServer(smtpURL);
        emailEditorController.setFrom(user);
        emailEditorController.setSendEmailHandle(sendEmail);
        lowerRightSplit.getChildren().add(htmlEditorView);
        count++;
    }
    
    /**
     * Utlity method for clearing the lower right pane if it is opened.
     */
    public void clearLowerRightPaneIfOpened(){
        //Check if the Email Web View is already opened
        if (count > 0) {
            clearLowerRightPane();
        }
    }
    
    /**
     * Event handler for the 'New Folder' button which displays a form for creating
     * a new folder in a popup window.
     * @param event 
     */
    private void displayNewFolderForm(MouseEvent event){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/NewFolderFormLayout.fxml"));
            AnchorPane newFolderView = (AnchorPane) loader.load();
            //Set the reference to the controller
            newFolderController = loader.getController();
            //Set the reference to EmailDAO in the controller
            newFolderController.setEmailDAO(emailDAO);
            //Send a reference to RootLayoutController to NewFolderFormLayoutController
            setRootControllerToNewFolderWindow();
            //Send a reference to FolderTreeLayoutController to NewFolderFormLayoutController
            newFolderController.setFolderTreeController(folderTreeController);
            //Send a reference to EmailTableLayoutController to NewFolderFormLayoutController
            newFolderController.setTableViewController(emailTableController);
            
            //Show the window
            folderStage = new Stage();
            folderStage.setScene(new Scene(newFolderView));
            //Set the window title and icon
            Image folderIcon = new Image(getClass().getResourceAsStream("/images/folder_icon.png"));
            folderStage.getIcons().add(folderIcon);
            folderStage.initStyle(StageStyle.UTILITY);
            folderStage.show();
        }
        catch(IOException e){
            LOG.error("Displaying the new folder form caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Button click handler for refreshing emails. It receives emails and adds
     * them to the database and display in the GUI.
     * @param event 
     */
    private void refreshEmails(MouseEvent event) throws SQLException, IOException{
        //Set the credentials for receiving email
        receiveEmail.setCredentials(user, password);
        receiveEmail.setImapServer(imapURL);
        //Get the new emails
        EmailData[] newEmails = null;
        boolean hasReceived = true;
        try{
            newEmails = receiveEmail.receiveEmail();
        }
        catch(MailException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle(null);
            alert.setContentText(resources.getString("ReceivingConfigErr"));
            alert.show();
            hasReceived = false;
        }
        if (hasReceived) {
            //If there are no new emails received
            if (newEmails.length < 1) {
                Alert dialog = new Alert(Alert.AlertType.INFORMATION, resources.getString("NoNewEmails"));
                dialog.setHeaderText(null);
                dialog.show();
            } //Add new emails to the database
            else {
                for (int i = 0; i < newEmails.length; i++) {
                    EmailData email = newEmails[i];
                    email.setFolderName("received");
                    email.setReceiveTime(LocalDateTime.now());
                    emailDAO.create(newEmails[i]);
                }
                //Display a message saying how many emails were retrieved
                Alert dialog = new Alert(Alert.AlertType.INFORMATION);
                dialog.setHeaderText(null);
                dialog.setContentText(newEmails.length + " " + resources.getString("EmailsRetrieved"));
                dialog.setTitle("New emails recieved");
                dialog.show();
                //Refresh and show the received folder
                emailTableController.displayTable("received");
            }
        }
    }
    
    /**
     * Method used to close the new folder window when it has been created 
     * successfully. Called in NewFolderFormLayoutController.
     */
    public void closeFolderFormWindow(){
        folderStage.close();
    }
    
    /**
     * Initialize the email web view so it is ready to be displayed dynamically
     * by the handler for a table item selection inside the bottom right split.
     */
    public void initEmailWebView() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/EmailWebViewLayout.fxml"));
            //Initialize the controller for the email web view
            this.emailView = (AnchorPane) loader.load();
            emailViewController = loader.getController();
        } catch (IOException e) {
            LOG.error("Displaying the email caused an error", e);
            Platform.exit();
        }
    }
    
    /**
     * Method used by the event handler for table selection to display the email
     * inside the lower right split and clear the split as necessary.
     */
    public void displayEmailWhenSelected() {
        //Disable email menu items
        sendEmailItem.setDisable(true);
        addAttachment.setDisable(true);
        removeAttachment.setDisable(true);
        //Clear lower right pane if needed
        clearLowerRightPaneIfOpened();
        //Display in lower right split
        lowerRightSplit.getChildren().add(emailView);
        count++;
    }
    
    /**
     * Pops up a message with information about the email application.
     */
    private void displayAboutWindow(MouseEvent e){
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setHeaderText(null);
        dialog.setTitle(null);
        dialog.setContentText(resources.getString("AboutTxt"));
        dialog.show();
    }
    
    /**
     * Method used to clear the lower right split.
     */
    public void clearLowerRightPane(){
        lowerRightSplit.getChildren().clear();
    }
    
    /**
     * Utility method for setting/updating the IMAP server
     * @param imap 
     */
    public void setIMAPServer(String imap){
        this.imapURL = imap;
    }
    
    /**
     * Utility method for setting/updating the SMTP server
     * @param smtp 
     */
    public void setSMTPServer(String smtp){
        this.smtpURL = smtp;
    }
}
