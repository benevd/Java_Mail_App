package com.dimitar.email_application_1634286.view;

import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import com.dimitar.business.MainAppFX;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import jodd.mail.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller class for the email view layout. It displays all the information
 * relevant to the currently selected email inside the table.
 *
 * @author Dimitar
 */
public class EmailWebViewLayoutController{
    private final static Logger LOG = LoggerFactory.getLogger(EmailWebViewLayoutController.class);
    //FXML fields
    @FXML
    private AnchorPane emailView;
    @FXML
    private Text fromField;
    @FXML
    private Text subjectField;
    @FXML
    private Text toField;
    @FXML
    private Text ccField;
    @FXML
    private Text receivedTimeField;
    @FXML
    private Text sentTimeField;
    @FXML
    private ComboBox openAttachBox;
    @FXML
    private WebView emailWebView;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Handle to the currently selected email
    private EmailData currentEmail;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        //Add an event listener for selection in the attachments combo box
        openAttachBox.getSelectionModel().selectedItemProperty().addListener( (observable, oldValue, newValue) -> {
            if(openAttachBox.getSelectionModel().getSelectedItem() != null){
                openAttachment((String) openAttachBox.getSelectionModel().getSelectedItem());
            }
        });
    } 
    
    /**
     * Open the selected attachment in the ComboBox inside a new window.
     * @param attachToDisplay 
     */
    private void openAttachment(String attachToDisplay){
        //Get the attachment bean
        AttachmentData attach = null;
        for(AttachmentData attachment : currentEmail.getAttachments()){
            if(attachment.getName().equals(attachToDisplay)){
                attach = attachment;
                break;
            }
        }
        //Open the attachment view
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(MainAppFX.class.getResource("/fxml/AttachmentViewLayout.fxml"));
            AnchorPane attachmentView = (AnchorPane) loader.load();
            AttachmentViewLayoutController attachmentViewController = loader.getController();
            attachmentViewController.displayAttachment(attach);
            
            //Show the window
            Stage attachStage = new Stage();
            attachStage.setScene(new Scene(attachmentView));
            attachStage.show();
        }
        catch(IOException e){
            LOG.error("Error loading the attachment view layout", e);
            Platform.exit();
        }
    }
    
    /**
     * Method invoked by the showEmail handler in the table layout controller to
     * set the information that will be displayed inside this view.
     * 
     * @param email 
     */
    public void setEmailDisplayInfo(EmailData email){
        //Set the current
        currentEmail = email;
        fromField.setText(email.getFrom());
        subjectField.setText(email.getSubject());
        toField.setText(recipientsToString(email, "to"));
        if(email.getCC() == null){
            ccField.setText("N/A");
        }
        else{
            ccField.setText(recipientsToString(email, "cc"));
        }
        if(email.getReceiveTime() == null){
            receivedTimeField.setText("N/A");
        }
        else{
            receivedTimeField.setText(email.getReceiveTime().toString());
        }
        if(email.getSendTime() == null){
            sentTimeField.setText("N/A");
        }
        else{
            sentTimeField.setText(email.getSendTime().toString());
        }
        //Load choice box with regular attachments
        loadRegularAttachments(email);
        
        //Load images from email
        loadHTML(email);
    }
    
    /**
     * Loads the list of regular attachments if there are any. Otherwise, 
     * the attachment combo box is disabled.
     * @param emailBean 
     */
    private void loadRegularAttachments(EmailData emailBean){
        if(emailBean.getAttachments() != null){
            //Clear the previous items
            openAttachBox.getItems().clear();
            //Enable the choicebox
            openAttachBox.setDisable(false);
            openAttachBox.setPromptText("");
            //Get the list of attachments and convert it to an observable list
            List<String> attachments = new ArrayList<>();
            for(AttachmentData attach : emailBean.getAttachments()){
                attachments.add(attach.getName());
            }
            ObservableList<String> attachmentsObservable = FXCollections.observableArrayList(attachments);
            openAttachBox.setItems(attachmentsObservable);
            
        }
        //If the email doesn't have regular attachments, don't show the choice box
        else{
            //Clear the previous items
            openAttachBox.getItems().clear();
            openAttachBox.setDisable(true);
            openAttachBox.setPromptText("No attachment");
        }
    }
    
    /**
     * Load the HTML web view. Saves the embedded images in the email if there 
     * are any or haven't been downloaded yet before.
     * @param emailBean 
     */
    private void loadHTML(EmailData emailBean){
        //Save the images if they aren't already
        if(emailBean.getEmbeddedAttachments() != null){
            saveEmbeddedImages(emailBean);
        }
        emailWebView.getEngine().loadContent(getModifiedHTML(emailBean));
    }
    
    /**
     * Utility method to modify an HTML string to be able to show embedded
     * attachments.
     * @param emailBean
     * @return 
     */
    private String getModifiedHTML(EmailData emailBean){
        String html = emailBean.getHtmlMessage();
        int index = html.indexOf("cid:");
        while (index >= 0) {
            StringBuffer sb = new StringBuffer(html);
            sb.replace(index, index+4, "file:/C:/Temp/");
            html = sb.toString();
            index = html.indexOf("cid:", index + 1); 
        }
        return html;
    }
    
    /**
     * Utility method to save embedded images to a temporary location so they
     * are ready to be displayed in the WebView. Only done for images that are
     * not already saved.
     * @param emailBean 
     */
    private void saveEmbeddedImages(EmailData emailBean){
        //Save all the embedded images to disk in the Temp folder
        for(AttachmentData embeddedImage: emailBean.getEmbeddedAttachments()){
            //Check if file already exists
            File img = new File("C:\\Temp", embeddedImage.getName());
            if(!img.exists()){
                try{
                    BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(embeddedImage.getAttachment()));
                    //Get the type of the current image
                    String fileName = embeddedImage.getName();
                    String type = fileName.substring(fileName.indexOf(".")+1, fileName.length());
                    ImageIO.write(bufferedImage, type, img);            
                }
                catch(IOException e){
                    LOG.error("Saving the file to temp has caused an error", e);
                    Platform.exit();
                }
            }
        }
    }
    
    /**
     * Helper method that creates a String representation from the EmailAddress
     * array of recipients depending on type (TO/CC).
     * @param email
     * @param type
     * @return the String representation of the EmailAddress[]
     */
    private String recipientsToString(EmailData email, String type){
        String recipients = "";
        String name;
        EmailAddress[] recipientsArr;
        if(type.equals("to")){
            recipientsArr = email.getTo();
        }
        else if(type.equals("cc")){
            recipientsArr= email.getCC();
        }
        else{
            recipientsArr = new EmailAddress[0];
            LOG.error("Wrong recipient type caused an error");
            Platform.exit();
        }
        for(EmailAddress addr : recipientsArr){
            if(addr.getPersonalName().isEmpty() || addr.getPersonalName() == null){
                name = resources.getString("UnavailableName");
            }
            else{
                name = addr.getPersonalName();
            }
            recipients += " "+name+" - "+addr.getEmail()+", ";
        }
        return recipients;
    }
    
}
