/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dimitar.email_application_1634286.view;

import com.dimitar.beans.EmailData;
import com.dimitar.persistence.EmailDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.collections.*;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * Controller class for the search bar and table view of emails. It is part of
 * the root layout container. It displays all the emails inside the current
 * folder selected in the folder tree view and allows to filter for an email
 * with a specific subject.
 *
 * @author Dimitar
 */
public class EmailTableLayoutController {
    
    private final static Logger LOG = LoggerFactory.getLogger(EmailTableLayoutController.class);
    
    //Reference to the email DAO object
    private EmailDAO emailDAO;
    
    //FXML fields
    @FXML
    private AnchorPane emailTable;
    
    @FXML
    private TextField searchBox;
    
    @FXML
    private TableView<EmailData> emailDataTable;
    
    @FXML
    private TableColumn<EmailData, String> subjectColumn;
    
    @FXML
    private TableColumn<EmailData, String> fromColumn;
    
    @FXML
    private TableColumn<EmailData, String> messageColumn;
    
    @FXML
    private TableColumn<EmailData, Number> priorityColumn;
    
    @FXML
    private TableColumn<EmailData, LocalDateTime> receivedColumn;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Controller handle for setting and displaying email information in the email view
    private EmailWebViewLayoutController emailViewController;
    private RootLayoutController rootController;
    
    //Current folder holder for searchbox
    private String currentFolder;
    
    //Context menu references for displaying on email selected
    ContextMenu menu;
    MenuItem delete;
    
    //List of folders to displays in the move submenu
    List<String> folders;
    
    //Keep track of the status of the context menu to open/close it dynamically
    boolean isMenuOpened;
    
    /**
     * Default constructor.
     */
    public EmailTableLayoutController(){
        super();
    }

    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        //Initialize the context menu and its items
        menu = new ContextMenu();
        delete= new MenuItem(resources.getString("DeleteEmail"));
        //Assign a handler to the delete menu item and add it to the context menu
        delete.setOnAction( (event) -> {
            deleteSelectedEmail();
        });
        Menu move = new Menu(resources.getString("MoveEmail"));
        
        menu.getItems().add(move);
        
        menu.getItems().add(delete);
        
        //Set the menu to closed by default
        isMenuOpened = false;
        
        //Connect each property of EmailData objects to its respective column in the table
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue().subjectProperty());
        fromColumn.setCellValueFactory(cellData -> cellData.getValue().fromProperty());
        receivedColumn.setCellValueFactory(cellData -> cellData.getValue().receiveTimeProperty());
        messageColumn.setCellValueFactory(cellData -> cellData.getValue().textMessageProperty());
        priorityColumn.setCellValueFactory(cellData -> cellData.getValue().messagePriorityProperty());
        
        //Listen for table selection changes and display the email corresponding to the selected row in the email web view layout
        emailDataTable.getSelectionModel().selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showEmail(newValue));
        //Set an event handler for when a row(email) is right-clicked to open a context menu
        emailDataTable.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent e){
                //If the context menu is opened, close it
                if(e.getButton() != MouseButton.SECONDARY && isMenuOpened){
                    menu.hide();
                }
                //Open the context menu at the position of the cursor only if the mouse event was a right click and that there is a selected item in the table
                if(e.getButton() == MouseButton.SECONDARY && !emailDataTable.getSelectionModel().isEmpty()){
                    menu.show(emailDataTable, e.getScreenX(), e.getScreenY());
                    isMenuOpened = true;
                }
            }
        });
        //Listen for search box changes and display emails that fit the search criteria as user types in
        searchBox.textProperty().addListener( (observable, oldValue, newValue) ->{
            try {
                displayEmailsByCriteria(newValue);
            } catch (SQLException | IOException e) {
               LOG.error("Connecting to the database or a database command has caused an error", e);
            }
        });
        //Initialize the list of folders
        folders = new ArrayList<>();
    }
    
    /**
     * Sets the list of folders with the most up to date folder data from the
     * database.
     */
    public void setFolders(){
        try {
            folders = emailDAO.findAllFolders();
            setMoveSubMenuItems();
        } catch (SQLException | IOException ex) {
           LOG.error("Error caused by the database", ex);
           Platform.exit();
        }
    }

    /**
     * Called in the root controller to gain access to the email DAO instance
     * to be able to read emails from the database.
     * @param emailDAO 
     */
    public void setEmailDAO(EmailDAO emailDAO){
        this.emailDAO = emailDAO;
    }
    
    /**
     * Displays the emails of the currently opened folder inside the table.
     * @param folder
     * @throws SQLException
     * @throws IOException 
     */
    public void displayTable(String folder) throws SQLException, IOException{
        //Clearing the selection and the search box before changing datasets/folder
        emailDataTable.getSelectionModel().clearSelection();
        searchBox.clear();
        //Set the current folder to be used by the search box
        currentFolder = folder;
        setMoveSubMenuItems();
        //Get the list of emails inside the folder    
        List<EmailData> emails = emailDAO.findFolder(folder);
        ObservableList<EmailData> emailsObservable = FXCollections.observableArrayList(emails);
        emailDataTable.setItems(emailsObservable);
    }
    
    /**
     * Helper method that updates the move folder sub menu items.
     */
    private void setMoveSubMenuItems(){
        //Set the move menu's sub items for each folder except the current one
        Menu move = (Menu) menu.getItems().get(0);
        //Remove the current folders inside the sub menu and add new ones
        move.getItems().clear();
        for(String folderName : folders){
            if (!folderName.equals(currentFolder)) {
                MenuItem folderToPlace = new MenuItem(folderName);
                //Set event handler
                folderToPlace.setOnAction( (event) -> {
                    moveEmail(folderName);
                });
                move.getItems().add(folderToPlace);
            }
        }
    }
    
    /**
     * Event handler for the Move email to folder sub-menu item.
     * @param newFolder 
     */
    private void moveEmail(String newFolder){
        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle(null);
        confirmation.setHeaderText(null);
        confirmation.setContentText(resources.getString("EmailFolderMoveMsg") +newFolder+"?");
        Optional<ButtonType> choice = confirmation.showAndWait();
        if (choice.get() == ButtonType.OK) {
            //Get the currently selected email
            EmailData emailToBeMoved = emailDataTable.getSelectionModel().getSelectedItem();
            try {
                emailDAO.updateFolder(emailToBeMoved.getID(), newFolder);
                //Refresh the table
                displayTable(currentFolder);
            } catch (SQLException e) {
                LOG.error("Deleting the folder has caused an error", e);
                Platform.exit();
            } catch (IOException e) {
                LOG.error("There was an issue with connecting to the database", e);
                Platform.exit();
            }
        }
    }
    
    /**
     * Display the emails of the currently opened folder who match the provided
     * criteria in the search box.
     * @param criteria
     * @throws SQLException
     * @throws IOException 
     */
    private void displayEmailsByCriteria(String criteria) throws SQLException, IOException{
        List<EmailData> emails = emailDAO.findSubject(criteria);
        //Removing emails who are not in the current folder from the list
        Iterator<EmailData> iterator = emails.iterator();
        while(iterator.hasNext()){
            EmailData email = iterator.next();
            if(!email.getFolderName().equals(currentFolder)){
                iterator.remove();
            }
        }
        ObservableList<EmailData> emailsObservable = FXCollections.observableArrayList(emails);
        emailDataTable.setItems(emailsObservable);
    }
    
    /**
     * Accessor for getting the table data. Used for clearing the selection to
     * avoid a NullPointerException when changing folders.
     * @return reference to the table view
     */
    public TableView<EmailData> getEmailDataTable(){
        return emailDataTable;
    }
    
    /**
     * Called in the root controller to gain access to the email view layout
     * controller so the emails can be opened from the table.
     * @param controller 
     */
    public void setEmailViewController(EmailWebViewLayoutController controller){
        this.emailViewController = controller;
    }
    
    /**
     * Called in the root controller to gain access to the root controller itself
     * so the table is able to listen for selections inside of it and open the 
     * appropriate email in the email view dynamically.
     * @param controller 
     */
    public void setRootLayoutController(RootLayoutController controller){
        this.rootController = controller;
    }
    
    /**
     * Event handler for when an email row has been selected in the table. It 
     * sets the appropriate info in the email view layout and displays it. If
     * there are no emails selected clear the pane.
     * @param email 
     */
    private void showEmail(EmailData email){
        //Close the context menu if it was opened
        if(menu != null && isMenuOpened){
            menu.hide();
        }
        //If an email is selected
        if(email != null){
            //Set the info that will be displayed
            emailViewController.setEmailDisplayInfo(email);
            //Display the view in the root layout bottom right split
            rootController.displayEmailWhenSelected();
        }
        //If none, clear the pane
        else{
            rootController.clearLowerRightPane();
        }
    }
    /**
     * Event handler for the 'Delete' context menu item in the email table. It 
     * deletes the email that is currently selected in the table after the user
     * has confirmed that they want to delete it.
     */
    private void deleteSelectedEmail() {
        //Get the currently selected email
        EmailData emailToBeDeleted = emailDataTable.getSelectionModel().getSelectedItem();
        //Pop a confirmation dialog so the user is certain that they want to delete the folder
        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle(null);
        confirmation.setHeaderText(null);
        confirmation.setContentText(resources.getString("DeleteEmailSubject")+" '" + emailToBeDeleted.getSubject() + "' "+resources.getString("DeleteEmailFrom")+ emailToBeDeleted.getFrom()+"?");
        Optional<ButtonType> choice = confirmation.showAndWait();
        if(choice.get() == ButtonType.OK){
            try{
                emailDAO.delete(emailToBeDeleted.getID());
                //Remove the email from the table
                emailDataTable.getItems().remove(emailToBeDeleted);
            }
            catch(SQLException e){
                LOG.error("Deleting the email has caused an error", e);
                Platform.exit();
            }
            catch(IOException e){
                LOG.error("There was an issue with connecting to the database", e);
                Platform.exit();
            }
        }
    }
}
