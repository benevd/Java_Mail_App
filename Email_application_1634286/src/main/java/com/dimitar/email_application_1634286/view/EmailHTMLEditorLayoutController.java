package com.dimitar.email_application_1634286.view;

import com.dimitar.beans.AttachmentData;
import com.dimitar.beans.EmailData;
import com.dimitar.business.SendEmail;
import com.dimitar.persistence.EmailDAO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import jodd.mail.EmailAddress;
import jodd.mail.MailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for the send email form & editor. It displays the form and HTML
 * editor to create an email bean and send the email and save it to the database
 * to the sent folder.
 *
 * @author Dimitar
 */
public class EmailHTMLEditorLayoutController{
    private final static Logger LOG = LoggerFactory.getLogger(EmailHTMLEditorLayoutController.class);
    
    //DAO handle
    private EmailDAO emailDAO;
    
    //EmailData bean that is bound to fields in the form & editor
    private EmailData email;
    
    //Lists of attachments to hold added attachments before sending the email
    private List<AttachmentData> regularAttachments;
    private List<AttachmentData> embeddedAttachments;
    
    //FXML fields
    @FXML
    private AnchorPane editorView;
    @FXML
    private BorderPane formView;
    @FXML
    private Button sendBtn;
    @FXML
    private MenuButton newAttachmentMenu;
    @FXML
    private MenuItem newRegularAttachment;
    @FXML
    private MenuItem newEmbeddedAttachment;
    @FXML
    private MenuButton removeAttachmentMenu;
    @FXML
    private TextField subjectField;
    @FXML
    private TextField toField;
    @FXML
    private TextField ccField;
    @FXML
    private TextField bccField;
    @FXML
    private TextField textMsgField;
    @FXML
    private HTMLEditor htmlEditor;
    
    //Resources
    @FXML
    private ResourceBundle resources;
    
    //Handle to send email
    private SendEmail send;
    
    //Current signed-in account email address
    private String from;
    
    //Handle to root controller to add attachments to the list of removable in the menu bar
    private RootLayoutController rootController;
    
    /**
     * Constructor that initializes the email bean.
     */
    public EmailHTMLEditorLayoutController(){
        super();
        
    }
    
    /**
     * Initializes the controller class. It does the binding of the fields to the
     * Email JavaFX bean.
     */
    @FXML
    public void initialize() {
        email = new EmailData();
        regularAttachments = new ArrayList<>();
        embeddedAttachments = new ArrayList<>();
        Bindings.bindBidirectional(subjectField.textProperty(), email.subjectProperty());
        Bindings.bindBidirectional(textMsgField.textProperty(), email.textMessageProperty());
        //Set the button handler for sending the email
        sendBtn.setOnMouseClicked((event) -> {
            try {
                this.sendEmail(event);
            } catch (SQLException | IOException e) {
                LOG.error("Connecting to the database or a database command caused an error", e);
                Platform.exit();
            }
        });
        //Set the Menu item handlers for adding and removing attachments
        newRegularAttachment.setOnAction( (event) -> {
            addAttachment("regular");
        });
        newEmbeddedAttachment.setOnAction( (event) -> {
            addAttachment("embedded");
        });
        
        //Set the Button menu for removing attachments to disabled by default
        removeAttachmentMenu.setDisable(true);
    }
    
    /**
     * Set the reference to the SendEmail object to send emails.
     * @param send 
     */
    public void setSendEmailHandle(SendEmail send){
        this.send = send;
    }
    
    /**
     * Set the value of the currently signed in email address so the application
     * knows where to send email from.
     * @param from 
     */
    public void setFrom(String from){
        this.from = from;
    }
    
    
    /**
     * Interprets email data that was not bound to a control and sends the email.
     * @param event 
     */
    public void sendEmail(MouseEvent event) throws SQLException, IOException{
        //Set the credential for user
        email.setFrom(from);
        boolean isSet = setEmailUnbindedData();
        //Convert the attachment lists to arrays if there are attachments
        if(!regularAttachments.isEmpty()){
            AttachmentData[] regularArr = regularAttachments.toArray(new AttachmentData[regularAttachments.size()]);
            email.setAttachments(regularArr);
        }
        if(!embeddedAttachments.isEmpty()){
            AttachmentData[] embeddedArr = embeddedAttachments.toArray(new AttachmentData[embeddedAttachments.size()]);
            email.setEmbeddedAttachments(embeddedArr);
        }
        
        if (isSet && email != null && send != null) {
            try {
                send.sendEmail(email);
                emailDAO.create(email);
                Alert dialog = new Alert(Alert.AlertType.INFORMATION, resources.getString("EmailSent"));
                dialog.setHeaderText(null);
                dialog.setTitle(null);
                dialog.show();
                //Clear the fields after the email was sent
                clearEmailFields();
            }
            catch (MailException e) {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setHeaderText(null);
                error.setTitle(null);
                error.setContentText(resources.getString("SendingConfigErr"));
                error.show();
            }
            //If send email has thrown an illegal argument exception, display the error message
            catch(IllegalArgumentException e){
                Alert dialog = new Alert(Alert.AlertType.ERROR);
                dialog.setHeaderText(null);
                dialog.setTitle(null);
                dialog.setContentText(resources.getString("ErrorSending"));
                dialog.show();
            }
        }
    }
    
    /**
     * Event handler for the menu items for adding attachments. It works with
     * both embedded and regular attachments.
     * @param type 
     */
    public void addAttachment(String type){
        //Check for valid type
        if(!type.equals("regular") && !type.equals("embedded")){
            LOG.error("ERROR - Wrong attachment type has been passed to the method");
            Platform.exit();
        }
        //Open the file explorer
        FileChooser chooser = new FileChooser();
        chooser.setTitle(resources.getString("ChooseAttachmentLocation"));
        File defaultDirectory = new File("C:/Temp");
        chooser.setInitialDirectory(defaultDirectory);
        File selectedFile = null;
        String selectedFilePath = null;
        try{
            selectedFile = chooser.showOpenDialog(null);
            selectedFilePath = selectedFile.toString();
        }
        catch(NullPointerException e){
            LOG.info("Chooser closed unexpectedly");
        }
        if(selectedFile != null && selectedFilePath != null){
            //Create the attachment bean from the file
            AttachmentData attach = new AttachmentData();
            //Get the file name
            int slashIndex = selectedFilePath.lastIndexOf("\\");
            String fileName = "";
            if(slashIndex != -1){
                fileName = selectedFilePath.substring(slashIndex + 1, selectedFilePath.length());
            }
            else{
                fileName = selectedFilePath;
            }
            //Set file name + path
            attach.setName(fileName);
            attach.setFilePath(selectedFilePath);
            
            //Set the byte array content for adding to database
            try(ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutput out = new ObjectOutputStream(bos);){
                out.writeObject(selectedFile);
                attach.setAttachment(bos.toByteArray());
            }
            catch(IOException e){
                LOG.error("Loading the image from the file has caused an error", e);
                Platform.exit();
            }
            
            
            //Add the attachment to the appropriate list
            if(type.equals("regular")){
                regularAttachments.add(attach);
                loadRemovableAttachments();
            }
            else{
                embeddedAttachments.add(attach);
                String htmlToEdit = htmlEditor.getHtmlText();
                StringBuilder sb = new StringBuilder(htmlToEdit);
                int lastBodyTagPos = sb.lastIndexOf("</body>");
                String htmlToAdd = "<img src='cid:"+fileName+"'>";
                sb.insert(lastBodyTagPos, htmlToAdd);
                htmlEditor.setHtmlText(sb.toString());
                loadRemovableAttachments();
            }
            //Enable the menu button for removing attachments
            removeAttachmentMenu.setDisable(false);
            rootController.setEnableRemoveAttachments(true);
        }
        
    }
    
    /**
     * Load the remove attachment into the menu of removable attachments and set
     * the click event handler.
     * @param type 
     */
    private void loadRemovableAttachments(){
        //Clear the MenuItems in editor and top menu bar
        removeAttachmentMenu.getItems().clear();
        rootController.clearRemovableAttachments();
        //Load all the regular attachments (if there are any)
        if (!regularAttachments.isEmpty()) {
            //Clear items
            for (AttachmentData attach : regularAttachments) {
                MenuItem attachToRemove = new MenuItem(attach.getName());
                attachToRemove.setGraphic(new ImageView(getClass().getResource("/images/regular_attach.png").toExternalForm()));
                //Set handler & add to the menu
                attachToRemove.setOnAction( (event) -> {
                    removeAttachment(attach, "regular", attachToRemove);
                });
                removeAttachmentMenu.getItems().add(attachToRemove);
                rootController.loadRemovableAttachment(attachToRemove);
            }
        }
        //Load all the embedded attachments (if there are any)
        if(!embeddedAttachments.isEmpty()){
            for (AttachmentData attach : embeddedAttachments) {
                MenuItem attachToRemove = new MenuItem(attach.getName());
                attachToRemove.setGraphic(new ImageView(getClass().getResource("/images/embedded_attach.png").toExternalForm()));
                //Set handler & add to the menu
                attachToRemove.setOnAction( (event) -> {
                    removeAttachment(attach, "embedded", attachToRemove);
                });
                removeAttachmentMenu.getItems().add(attachToRemove);
                rootController.loadRemovableAttachment(attachToRemove);
            }
        }
    }
    
    /**
     * Handler for removing an attachment from the email when an attachment is
     * selected in the menu button.
     * @param attach
     * @param type
     * @param item 
     */
    public void removeAttachment(AttachmentData attach, String type, MenuItem item){
        if(type.equals("regular")){
            //Remove the attachment from the list
            regularAttachments.remove(attach);
            //Remove the attachment from the button menu
            removeAttachmentMenu.getItems().remove(item);
            
        }
        else if(type.equals("embedded")){
            //Remove the attachment from the list
            embeddedAttachments.remove(attach);
            //Remove the attachment from the button menu
            removeAttachmentMenu.getItems().remove(item);
        }
        else{
            LOG.error("ERROR - Wrong attachment type has been passed to the method");
        }
        //If both attachment lists are empty set the MenuButton to disabled
        if(regularAttachments.isEmpty() && embeddedAttachments.isEmpty()){
            removeAttachmentMenu.setDisable(true);
            rootController.setEnableRemoveAttachments(false);
        }
    }
    
    /**
     * Helper method used to clear all the email send form fields. Also used in
     * the root controller to clear the fields of the email when clicking the
     * button for creating a new email. 
     */
    public void clearEmailFields(){
        subjectField.clear();
        toField.clear();
        ccField.clear();
        bccField.clear();
        textMsgField.clear();
        htmlEditor.setHtmlText("");
        clearAllAttachments();
    }
    
    /**
     * Utility method for clearing all attachments when an user wants to create
     * a new email.
     */
    private void clearAllAttachments(){
        if(embeddedAttachments != null && !embeddedAttachments.isEmpty()){
            embeddedAttachments.clear();
            removeAttachmentMenu.getItems().clear();
            removeAttachmentMenu.setDisable(true);
            rootController.clearRemovableAttachments();
            rootController.setEnableRemoveAttachments(false);
        }
        if(regularAttachments != null && !regularAttachments.isEmpty()){
            regularAttachments.clear();
            removeAttachmentMenu.getItems().clear();
            removeAttachmentMenu.setDisable(true);
            rootController.clearRemovableAttachments();
            rootController.setEnableRemoveAttachments(false);
        }
    }
    
    /**
     * Helper method that reads the fields that were not binded to the JavaFX
     * email bean and sets them 
     */
    private boolean setEmailUnbindedData(){
        //Get the recipients and remove all white spaces so it can be split easily
        String toList = toField.getText().trim().replaceAll("\\s+", "");
        if(toList.isEmpty()){
            Alert dialog = new Alert(Alert.AlertType.WARNING, resources.getString("EmptyTo"));
            dialog.show();
            return false;
        }
        else{
            //Create an EmailAddress array from the recipients and place in email bean
            String[] recipients = toList.split(",");
            EmailAddress[] recipientsAddr = new EmailAddress[recipients.length];
            EmailAddress addr;
            for(int i=0; i<recipients.length; i++){
                addr = new EmailAddress("", recipients[i]);
                recipientsAddr[i] = addr;
            }
            email.setTo(recipientsAddr);
            //Get the CC addresses if there are any
            String ccList = ccField.getText().trim().replaceAll("\\s+", "");
            if(!ccList.isEmpty()){
                String[] cc = ccList.split(",");
                EmailAddress[] ccAddr = new EmailAddress[cc.length];
                for (int i = 0; i < cc.length; i++) {
                    addr = new EmailAddress("", cc[i]);
                    ccAddr[i] = addr;
                }
                email.setCC(ccAddr);
            }
            //Get the BCC addresses if there are any
            String bccList = bccField.getText().trim().replaceAll("\\s+", "");
            if(!bccList.isEmpty()){
                String[] bcc = bccList.split(",");
                EmailAddress[] bccAddr = new EmailAddress[bcc.length];
                for (int i = 0; i < bcc.length; i++) {
                    addr = new EmailAddress("", bcc[i]);
                    bccAddr[i] = addr;
                }
                email.setBCC(bccAddr);
            }
            //Get the HTML message
            String htmlMsg = htmlEditor.getHtmlText();
            email.setHtmlMessage(htmlMsg);
            //Set the folder to sent
            email.setFolderName("sent");
            //Set the sent time to now
            email.setSendTime(LocalDateTime.now());
            //Set the priority to 1
            email.setMessagePriority(1);
        }
        return true;
    }

    /**
     * Called in the root controller to gain access to the email DAO instance
     * in order to be able to save sent emails to the database.
     * @param emailDAO 
     */
    public void setEmailDAO(EmailDAO emailDAO){
        this.emailDAO = emailDAO;
    }
    
    /**
     * Set a reference to the Root Controller.
     * @param controller 
     */
    public void setRootController(RootLayoutController controller){
        this.rootController = controller;
    }
}
